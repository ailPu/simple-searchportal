<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClothingCondition extends Model
{
    use HasFactory;

    protected $fillable = ["condition"];

    public function ad()
    {
        return $this->belongsTo(\App\Models\Ad::class, "id", "condition_id");
    }
}
