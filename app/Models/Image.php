<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $fillable = ["ad_id", "order_id", "name"];

    public function ad()
    {
        return $this->belongsTo(\App\Models\Ad::class, "ad_id", "id");
    }
}
