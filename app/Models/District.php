<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;

    public function ad()
    {
        return $this->belongsTo(\App\Models\Ad::class, "district_id", "id");
    }
}
