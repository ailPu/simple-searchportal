<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;


    // TODO guarded, weil ich ja jetzt mehreres in die Datenbank speichern muss!
    protected $guarded = [];
    // protected $fillable = ['message'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
