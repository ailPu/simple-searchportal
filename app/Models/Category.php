<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ["parent_id", "name", "bottom_category"];

    public function isTopCategory()
    {
        return $this->belongsTo(\App\Models\Category::class, "parent_id", "id")->where("parent_id", null);
    }

    public function parent()
    {
        return $this->belongsTo(\App\Models\Category::class, "parent_id", "id");
    }

    public function children()
    {
        return $this->hasMany(\App\Models\Category::class, "parent_id", "id");
    }

    public function attributes()
    {
        return $this->belongsToMany(
            Attribute::class,
            'categories_attributes',
            'category_id',
            'attribute_id'
        );
    }
}
