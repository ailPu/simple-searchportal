<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    protected $fillable = ["table_name"];

    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'categories_attributes',
            'attribute_id',
            'category_id'
        );
    }

    public function ad()
    {
        // return
    }
}
