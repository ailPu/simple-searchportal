<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ad extends Model
{
    use HasFactory;

    protected $fillable = ["user_id", "district_id", "condition_id", "color_id", "size_id", "shoe_size_id", "waist_size_id", "category_path", "name", "description", "price", "released", "sold"];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, "user_id", "id");
    }

    public function imgs()
    {
        return $this->hasMany(\App\Models\Image::class, "ad_id", "id")->orderBy("order_id", "asc");
    }

    public function img()
    {
        // TODO hasOne oder hasMany? orderBy am Ende, weil ich ja noch die order_id brauche
        return $this->hasOne(\App\Models\Image::class, "ad_id", "id")->orderBy("order_id", "asc");
    }

    public function district()
    {
        return $this->hasOne(\App\Models\District::class, "id", "district_id");
    }

    public function color()
    {
        return $this->hasOne(\App\Models\Color::class, "id", "color_id");
    }

    public function condition()
    {
        return $this->hasOne(\App\Models\ClothingCondition::class, "id", "condition_id");
    }

    public function shoeSize()
    {
        return $this->hasOne(\App\Models\ShoeSize::class, "id", "shoe_size_id");
    }

    public function waistSize()
    {
        return $this->hasOne(\App\Models\WaistSize::class, "id", "waist_size_id");
    }

    public function size()
    {
        return $this->hasOne(\App\Models\Size::class, "id", "size_id");
    }
}
