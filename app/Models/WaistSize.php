<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WaistSize extends Model
{
    use HasFactory;

    protected $fillable = ["size"];

    public function ad()
    {
        return $this->belongsTo(\App\Models\Ad::class, "id", "waist_size_id");
    }
}
