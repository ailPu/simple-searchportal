<?php

namespace App\Traits;

use Exception;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


/**
 * Trait Image Upload resize max. Breite 800 & Quadrat 150px
 * 
 *  */
trait UploadImage
{

    /**
     * @var string
     */
    protected $traitOriginalFilename;

    /**
     * @var string
     */
    private $insertPath;

    /**
     * @var string
     */
    protected $fileUrl;

    /**
     * Entfernt alle Zeichen außer a-Z, 0-9 . und \ aus dem Filename
     */
    protected function checkFile()
    {
        if (!isset($this->inputFile) || !isset($this->diskName)) {
            throw new \Exception("Kein Property 'inputFile' oder 'diskName' definiert!");
        }
        // die request() ist eine helper funktion, die global verfügbar ist. $this->inputFile kommt aus der Klasse. Wir wollen es ja variabel für mehrere Klassen haben

        // TODO gegenwärtig auf EINEN Upload zugeschnitten. $this->inputFile ist eben der key von der Klasse, zB "image"
        // TODO ich habe jetzt aber "images". Der Trait muss also alle Schritte wiederholt durchführen 
        $originalFileName = preg_replace('![^a-z0-9\.]!i', "_", request()->file($this->inputFile)->getClientOriginalName());
        // $this->diskName kommt wieder aus der Klasse, die diesen Trait verwendet
        $fileExists = Storage::disk($this->diskName)->exists($originalFileName);
        // Wenn Datei schon existiert, dann hänge timestamp hinten dran
        if ($fileExists) {
            $originalArray = pathinfo($originalFileName);
            $originalFileName = $originalArray["filename"] . time() . "." . $originalArray["extension"];
        }
        $this->traitOriginalFilename = $originalFileName;
    }

    /**
     * speichert die Originaldatei ab
     */
    protected function storeFile()
    {
        // Falls diese Methode direkt aufgerufen wird, dann schau' erst, ob der traitOriginalFilename existiert
        //TODO Lade ich mehrere Bilder rauf, dann ist traitOriginalFileName nach dem ersten Foto besetzt und ich überschreibe alle Bilder und lade zwar alle hoch, aber eben habe ich dann nur eines! 
        //TODO $this->traitOriginalFilename gehört genulled, wenn fertig!
        if (!$this->traitOriginalFilename) $this->checkFile();
        // if (!$this->traitOriginalFilename) $this->checkFile();
        request()->file($this->inputFile)->storeAs("", $this->traitOriginalFilename, $this->diskName);
    }

    /**
     * Verkleinert das Bild auf max. Breite
     * @param int $width
     * @param mixed string/false [default false] prefix Dateiname
     */

    protected function resizeMaxWidth(int $width, $prefix = false)
    {
        if (!$this->traitOriginalFilename) $this->checkFile();
        $img = Image::make(request()->file($this->inputFile))->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $this->setPath();
        $img->save($this->insertPath . '/' . $prefix . $this->traitOriginalFilename);
    }

    /**
     * Verkleinert das Bild und macht ein Quadrat daraus
     * @param int $width
     * @param mixed string/false [default false] prefix Dateiname
     */

    protected function square(int $width, $prefix = false)
    {
        if (!$this->traitOriginalFilename) $this->checkFile();
        $img = Image::make(request()->file($this->inputFile))->fit($width);
        $this->setPath();
        $img->save($this->insertPath . '/' . $prefix . $this->traitOriginalFilename);
    }

    /**
     * Variablen aus config/filesystems.php ->disk 
     */
    private function setPath()
    {
        if (!isset($this->diskName)) {
            throw new \Exception("Kein Property 'inputFile' oder 'diskName' definiert!");
        }
        // Wohin wir speichern kann ja wieder von der Klasse abhängig sein. Achtung auf Punkte! Diese braucht es ja, weil so die Ordner angesprochen werden (filesystems.disks.whatever.root). Also wo Datei abgespeichert werden soll
        $this->insertPath = config("filesystems.disks." . $this->diskName . ".root");
        // Für den Dateipfad in HTTP:://
        $this->fileUrl = config("filesystems.disks." . $this->diskName . ".url");
    }

    /**
     * Dateipfad auslesen
     * @return string $this->fileUrl
     */

    protected function getPath()
    {
        $this->setPath();
        return $this->fileUrl;
    }

    /**
     * Serverpfad auslesen
     * @return string $this->insertPath
     */

    protected function getServerPath()
    {
        $this->setPath();
        return $this->insertPath;
    }

    /**
     * @var string $filename
     * @return 
     */

    protected function traitFileOutput($filename)
    {
        $file = $this->getServerPath() . '/' . $filename;
        if (file_exists($file)) return response()->file($file);
        return response("File not found", 404);
    }

    /**
     * @var string $filename
     * @return 
     */

    protected function traitFileDownload($filename)
    {
        $file = $this->getServerPath() . '/' . $filename;
        // Man muss nur "download" schreiben anstatt von "file" wie in der traitFileOutput Methode
        if (file_exists($file)) return response()->download($file);
        return response("File not found", 404);
    }

    /**
     * Dateien löschen
     * @param string $filename
     * @param mixed string/array/false $prefix
     */
    protected function dropImages($filename, $prefix = false)
    {
        // Setze von Haus aus die Standarddatei hinein in das array
        $file = $this->getServerPath() . "/" . $filename;
        if (file_exists($file)) {
            $files = [$filename];
            if (is_array($prefix)) {
                foreach ($prefix as $pre) {
                    $files[] = $pre . $filename;
                }
            }
            // Wenn prefix zB nur big_ oder prev_ 
            else if (is_string($prefix)) {
                // todo checken
                $files[] = $prefix . $filename;
            }
        }
        Storage::disk($this->diskName)->delete($files);
    }
}
