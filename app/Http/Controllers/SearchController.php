<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Category;
use App\Traits\UploadImage;
use Dotenv\Repository\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Symfony\Component\Console\Input\Input;

class SearchController extends Controller
{

    use UploadImage;

    /** @var string Form input name */
    protected $inputFile =  "image";

    /** @var string config filesystem disks->index*/
    protected $diskName = "public_image";

    /**
     * Display Indexpage for searching 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $root = Category::with("children")->where("parent_id", null)->first();
        return view("search.index", compact("root"));
    }

    /**
     * Show results for given parameters
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse
     */
    public function results(Request $request)
    {
        if ($request->missing("keyword") && request("currLevel") == 0 || $request->isNotFilled("keyword") && request("currLevel") == 0) {
            return redirect()->route("ad.index");
        }

        if ($request->ajax()) {
            if (request("atTopLevel")) $results = $this->getResultsAtTopCategory();
            elseif (request("atSubLevel")) $results = $this->getResultsAtSubCategory();
            return response()->json($results);
        }

        $menuHtml = $this->createMenuHtml($this->getPreviousMenus(), $this->createSubMenuHtmlForKeyword($this->getSubMenusForKeyword()));

        $adsPaginated = Ad::with("img")->where([
            ["name", "LIKE", DB::raw('CONCAT("%",:keyword,"%")')],
            ["category_path", "LIKE", DB::raw('CONCAT(:category_path,"%")')],
            ["released", "=", DB::raw(":released")],
        ])
            ->setBindings(["keyword" => request("keyword") ?? "", "category_path" => request("category_path") ?? "", ":released" => 1])
            ->paginate(10);

        $urlWithParams = URL::full();
        $urlWithoutParams = URL::current();
        $imgPath = $this->getPath();

        return view("search.results", compact("menuHtml", "urlWithParams", "urlWithoutParams", "adsPaginated", "imgPath"));
    }

    /**
     * Fetch matching Topcategories for given parameters when using ajax
     * @return array
     */
    public function getResultsAtTopCategory()
    {
        // TODO TEST
        return AD::join('categories as c_first_level', function ($join) {
            $join->on('c_first_level.id', '=', DB::raw('SUBSTRING_INDEX(ads.category_path,",",1)'));
        })
            ->selectRaw("COUNT(DISTINCT(ads.id)) as cat_count, CONCAT(SUBSTRING_INDEX(ads.category_path,',',1),',') as category_path, LENGTH(SUBSTRING(ads.category_path,1,1)) as currLevel, c_first_level.name as first_level_category_name, c_first_level.id as first_level_category_id")
            ->where([
                ["ads.name", "LIKE", DB::raw("CONCAT('%',:keyword,'%')")],
                ["ads.released", "=", DB::raw(":released")]
            ])
            ->setBindings(["keyword" => request("keyword") ?? "", "released" => 1])->groupBy("c_first_level.id")->get();
    }

    /**
     * Fetch matching Subcategories for given parameters when using ajax
     * @return array
     */
    public function getResultsAtSubCategory()
    {

        return AD::join('categories as c_deepest', function ($join) {
            $join->on('c_deepest.id', '=', DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(ads.category_path,",",-2),",",1)'));
        })
            ->join("categories as c_one_before_deepest", function ($join) {
                $join->on('c_one_before_deepest.id', '=', DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(ads.category_path,",",-3),",",1)'));
            })
            ->join("categories as c_second_highest", function ($join) {
                $join->on('c_second_highest.id', '=', DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(ads.category_path,",",2),",",-1)'));
            })
            ->selectRaw("COUNT(DISTINCT(ads.id)) as cat_count, LENGTH(ads.category_path)  - LENGTH(REPLACE(ads.category_path,',','')) as currLevel,ads.name, ads.category_path as category_path,c_second_highest.name as second_highest_category_name, c_second_highest.id as second_highest_category_id, c_deepest.name as deepest_category_name, c_deepest.id as deepest_category_id, c_one_before_deepest.name as one_before_deepest_category_name, c_one_before_deepest.id as one_before_deepest_category_id")
            ->where([
                ["ads.name", "LIKE", DB::raw("CONCAT('%',:keyword,'%')")],
                ["ads.category_path", "LIKE", DB::raw('CONCAT(:category_path,"%")')],
                ["ads.released", "=", DB::raw(":released")]
            ])
            ->setBindings(["keyword" => request("keyword") ?? "", "category_path" => request("category_path") ?? "", "released" => 1])
            ->groupBy("c_deepest.id")
            ->get();
    }

    /**
     * Fetch previous Menus for creating visible hierarchy
     * @return array
     */
    public function getPreviousMenus()
    {

        //! Fallback for older Serverversions 
        // return DB::select("SELECT c2.id, c2.name,c2.parent_id
        // FROM (
        // SELECT @r AS _id,(SELECT @r := parent_id FROM categories WHERE id = _id) AS parent_id
        // FROM(SELECT @r := :currCatId, @l := 0) vars,categories h
        // WHERE @r IS NOT NULL) c1
        // JOIN categories c2
        // ON c1._id = c2.id", ["currCatId" => request("currCatId") ?? Category::where("parent_id", null)->first()->id]);

        return DB::select("
        WITH RECURSIVE category_path (id, name, parent_id) AS
        (
            SELECT id, name, parent_id
            FROM categories
            WHERE id = :currentId
            UNION ALL
            SELECT c.id, c.name, c.parent_id
            FROM category_path AS cp JOIN categories AS c
            ON c.id = cp.parent_id
            )
            SELECT * FROM category_path", ["currentId" => request("currCatId") ?? Category::where("parent_id", null)->first()->id]);
    }

    /**
     * Fetch matching Submenus for given Parameters
     * @return array
     */
    public function getSubMenusForKeyword()
    {
        return AD::join("categories as c", function ($join) {
            $join->on('c.id', '=', DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(ads.category_path,",",:level),",",-1)'));
        })
            ->selectRaw("COUNT(DISTINCT(ads.id)) as cat_count, ads.category_path as whole_category_path, c.name as cat_name,c.id as category_id")
            ->where([
                ["ads.category_path", "LIKE", DB::raw("CONCAT(:category_path,'%')")],
                ["ads.name", "LIKE", DB::raw("CONCAT('%',:keyword,'%')")],
                ["ads.released", "=", DB::raw(":released")]

            ])
            ->setBindings(["level" => request("currLevel") + 1, "category_path" => request("category_path") ?? "", "keyword" => request("keyword") ?? "", "released" => 1])
            ->groupBy("c.id")
            ->get();
    }

    /**
     * Create HTML for found submenus
     * @param array $subMenus
     * @return string $subMenuHtml
     */
    public function createSubMenuHtmlForKeyword($subMenus)
    {
        $searchRoute = URL::current();
        $subMenuHtml = "<ul>";
        foreach ($subMenus as $subMenu) {
            $levelForUrl = request("currLevel") + 1;
            $category_pathForUrl = request("category_path") . $subMenu->category_id . ",";
            $li = "<li><a href='$searchRoute?category_path=" . $category_pathForUrl . "&currCatId=" . $subMenu->category_id . "&currLevel=" . $levelForUrl . "&keyword=" . request('keyword') . "'>" . $subMenu->cat_name . " (" . $subMenu->cat_count . ")</a></li>";
            $subMenuHtml .= $li;
        }
        $subMenuHtml .= "</ul>";
        return $subMenuHtml;
    }

    /**
     * Create whole HTML Menu from Top to bottom for given search parameters
     * @param array $currentCategoryToTop
     * @param string $subMenuHtml
     * @return string
     */
    public function createMenuHtml($currentCategoryToTop, $subMenuHtml)
    {
        $level = request("currLevel");
        $category_path = request("category_path") ?? "";
        $searchRoute = URL::current();
        $loopLevel = 0;
        $topLevel = count($currentCategoryToTop);
        $atCurrentLevel = true;
        $wholeMenu = "";
        $category_pathTrimmed = "";
        foreach ($currentCategoryToTop as $category) {
            $loopLevel++;
            if ($atCurrentLevel) {
                $wholeMenu = "<ul><li><span>" . $category->name . "</span>
                    " . $subMenuHtml . "</li></ul>";
                $atCurrentLevel = false;
            } else if ($loopLevel == $topLevel) {
                $wholeMenu = "<ul><li><a href='$searchRoute?category_path=&currLevel=" . $level . "&currCatId=" . $category->id . "&keyword=" . request('keyword') . "'>" . $category->name . "</a>
                    " . $wholeMenu . "</li></ul>";
            } else {
                $wholeMenu = "<ul><li><a href='$searchRoute?category_path=" . $category_pathTrimmed . "&currLevel=" . $level . "&currCatId=" . $category->id . "&keyword=" . request('keyword') . "'>" . $category->name . "</a>
                    " . $wholeMenu . "</li></ul>";
            }
            $category_path = substr($category_path, 0, strrpos($category_path, ","));
            $category_pathTrimmed = substr($category_path, 0, strrpos($category_path, ",") + 1);

            $level--;
        }
        return $wholeMenu;
    }

    // /**
    //  * Fetch img for display in view
    //  * @param $img 
    //  * @return output Image
    //  */
    // public function outputImg($img)
    // {
    //     return $this->traitFileOutput($img);
    // }
}
