<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Events\MessageSent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ChatController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('chat.index');
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages(User $user)
    {
        return Message::with('user')
            ->where(["user_id" => Auth::id(), "receiver_id" => $user->id])
            ->orWhere(["user_id" => $user->id, "receiver_id" => Auth::id()])
            ->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request, User $user)
    {

        $text = request("message");
        $message = Message::create(["user_id" => Auth::id(), "message" => $text, "receiver_id" => $user->id]);

        broadcast(new MessageSent(Auth::user(), $message))->toOthers();

        return ['status' => 'Message Sent!', "message" => $request->all()];
    }
}
