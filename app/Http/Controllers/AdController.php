<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Attribute;
use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\ClothingCondition;
use App\Models\Color;
use App\Models\District;
use App\Models\Image;
use App\Models\ShoeSize;
use App\Models\Size;
use App\Models\WaistSize;
use App\Traits\UploadImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use ReflectionClass;
use stdClass;

class AdController extends Controller
{
    use UploadImage;

    /** @var string Form input name */
    protected $inputFile;

    /** @var string config filesystem disks->index*/
    protected $diskName = "public_image";

    public function __construct()
    {
        $this->middleware(['auth', 'can:registered',])->only("create");
    }

    /**
     * Show all Topcategories
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // TODO erstes Bild der Anzeigen holen zum Anzeigen
        $adsPaginated = Ad::with("img")->where("released", 1)->paginate(10);
        $topCategories = Category::whereHas("isTopCategory")
            ->leftJoin('ads as a', function ($join) {
                $join->on('categories.id', 'LIKE', DB::raw('SUBSTRING_INDEX(a.category_path,",",1)'))->where("a.released", 1);
            })
            ->selectRaw("categories.*,COUNT(DISTINCT(a.id)) as cat_count")->groupBy("categories.id")->get();

        return view("ad.index", compact("topCategories", "adsPaginated"));
    }

    /** 
     * Fetch subCategories for given categoryId
     */
    public function fetchSubcategories(Request $request)
    {
        $subCategories = Category::where("parent_id", request()->input("prevCategoryId"))->get();
        return response()->json($subCategories);
    }

    /**
     * Fetch Attributes for bottom_categoryId
     * 
     */
    public function fetchAttributes(Request $request)
    {
        // unset previously assigned validationRules (if user changed categories)
        session()->forget("dynamicValidationRules");

        session()->put("dynamicValidationRules", []);

        $attributeTables = Category::findOrFail($request->categoryId)->attributes;
        $attributesData = [];
        foreach ($attributeTables as $attributeTable) {
            $this->setValidationRules($attributeTable->table_name);
            $attributesData[$attributeTable->table_name] = DB::table($attributeTable->table_name)->get();
        }


        $conditions = ClothingCondition::get();
        foreach ($conditions as $condition) {
            $attributesData["conditions"][] = $condition;
        }

        $headers = ["colors" => "Farbe", "sizes" => "Größe", "shoe_sizes" => "Schuhgröße", "waist_sizes" => "Bundweite", "conditions" => "Zustand"];
        $districts = $this->fetchDistricts();
        return response()->json(["attributes" => $attributesData, "districts" => $districts, "headers" => $headers]);
    }

    /**
     * Dynamically set Validationrules when attributes are being fetched
     */
    public function setValidationRules($tableName)
    {
        $validationRule = substr($tableName, 0, strlen($tableName) - 1) . "_id";
        session()->push("dynamicValidationRules", $validationRule);
    }

    public function fetchDistricts()
    {
        return District::get();
    }

    /**
     * Show the first part of the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topCategories = Category::whereHas("isTopCategory")->get();
        return view("ad.create_details", compact("topCategories"));
    }

    /**
     * Show the second part of the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectImgs($id)
    {
        $ad = AD::where(
            ["user_id" => Auth::id(), "released" => 0]
        )
            ->findOrFail($id);
        return view("ad.create_imgs", compact("ad"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // TODO rename in store ad infos (no images)
    public function store(Request $request)
    {
        //! Always present rules 
        $request->validate([
            "name" => "required|min:3",
            "price" => "required|integer|min:0|max:99999",
            "price-comma" => "required|integer|min:0|max:99",
            "topCategory" => "required",
            "subCategories.*" => "required",
            "condition_id" => "required|exists:\App\Models\ClothingCondition,id",
            "district_id" => "required|exists:\App\Models\District,id",
        ]);

        // check which attributes have been set in session to validate
        $this->validateDynamicValidationRules($request);

        $categoryPath = $this->checkCategoryHierarchy($request->subCategories);

        $sessionAtributes = session("dynamicValidationRules");
        $attributes = [];
        foreach ($sessionAtributes as $sessionAtribute) {
            $attributes[$sessionAtribute] = request($sessionAtribute);
        }
        $price = request("price") . "." . request("price-comma");
        $basicData = ["user_id" => Auth::id(), "category_path" => $categoryPath, "name" => request("name"), "condition_id" => request("condition_id"), "district_id" => request("district_id"), "price" => $price, "description" => request("description")];
        $dataForAd = array_merge($basicData, $attributes);

        $ad = new Ad($dataForAd);
        $ad->save();
        session()->forget("dynamicValidationRules");

        //TODO redirect to route with GET Parameter! ID from created ad! Otherwise images will be uploaded anywhere 
        return redirect()->route("ad.selectImgs", $ad->id);
    }

    public function storeImg($key)
    {
        // assign property to $key from form so trait can access file
        $this->inputFile = $key;
        try {
            $this->storeFile();
            $this->resizeMaxWidth(800, "big_");
            $this->resizeMaxWidth(300, "prev_");
        } catch (\Exception $e) {
            if (config("app.debug")) abort("580", $e->getMessage() . ' In Zeile ' . $e->getLine() . '. In Datei ' . $e->getFile());
            abort("580", "Server Fehler, Datei kann nicht gespeichert werden");
        }
        $imgName = $this->traitOriginalFilename;

        // without nulling, each file would be overwritten and only last img would stay 
        $this->traitOriginalFilename = null;

        $order_id = substr($key, strrpos($key, "_") + 1);
        $img = new Image(["ad_id" => request("ad_id"), "order_id" => $order_id, "name" => $imgName]);
        $img->save();
    }

    // TODO rename eher in store Images
    public function storeImgs(Request $request)
    {
        //TODO ajax error validieren? 
        Ad::where([
            "id" => request("ad_id"), "user_id" =>  Auth::id(), "released" => 0,
        ])->findOrFail(request("ad_id"));

        if (request("image_0")) {
            foreach ($request->all() as $key => $image) {
                if (!Str::contains($key, "image")) continue;
                $this->storeImg($key);
            }
        }

        return response()->json(["successRoute" => route("ad.showPreview", request("ad_id"))]);
    }

    public function updateImgs(Request $request)
    {
        $ad = Ad::with("imgs")->where(["id" => request("ad_id"), "user_id" =>  Auth::id()])->findOrFail(request("ad_id"));

        $imgsToRemove = [];
        foreach ($ad->imgs as $img) {
            $imgsToRemove[$img->name] = $img->name;
        }

        if (request("image_0")) {
            foreach ($request->all() as $key => $image) {
                if (!Str::contains($key, "image")) continue;

                // existing picture which isn't a file object or if user updated Imgs but didn't refresh page ($().data() still contains complete file)
                if (is_string($image) || isset($imgsToRemove[preg_replace('![^a-z0-9\.]!i', "_", $image->getClientOriginalName())])) {
                    $order_id = substr($key, strrpos($key, "_") + 1);

                    $imageName = is_string($image) ? $image : preg_replace('![^a-z0-9\.]!i', "_", $image->getClientOriginalName());

                    Image::where("name", $imageName)->update(["order_id" => $order_id]);
                    unset($imgsToRemove[$imageName]);
                } else {
                    $this->storeImg($key);
                }
            }
        }

        foreach ($imgsToRemove as $imgToRemove) {
            Image::where([
                "ad_id" => $ad->id, "name" => $imgToRemove
            ])->delete();
            $this->dropImages($imgToRemove, ["prev_", "big_"]);
        }

        if ($ad->released == 1) return response()->json(["successRoute" => route("ad.show", $ad->id)]);
        else return response()->json(["successRoute" => route("ad.showPreview", $ad->id)]);
    }

    /**
     * Ask user for releasing ad. Show Preview
     */
    public function showPreview(Request $request, $id)
    {
        $ad = AD::with("imgs")->where(["user_id" => Auth::id(), "released" => 0])->findOrFail($id);
        return view("ad.preview", compact("ad"));
    }

    /**
     * Release ad
     */
    public function releaseAd($id)
    {
        $ad = AD::where(["user_id" => Auth::id(), "id" => $id, "released" => 0])->findOrFail($id);
        $ad->update(["released" => 1]);

        return redirect()->route("ad.index")->with("success", "Deine Anzeige wurde erfolgreich veröffentlicht");
    }

    public function validateDynamicValidationRules(Request $request)
    {
        // "color_id" => "exists:\App\Models\Color,id",
        // "size_id" => "exists:\App\Models\Size,id",
        // "waist_size_id" => "exists:\App\Models\WaistSize,id",
        // "shoe_size_id" => "exists:\App\Models\ShoeSize,id",

        $models = [
            "color_id" => "Color",
            "size_id" => "Size",
            "waist_size_id" => "WaistSize",
            "shoe_size_id" => "ShoeSize",
        ];


        foreach (session("dynamicValidationRules") as $field) {
            // dd("VALIDATE", $field);
            // dd($request->all(), $field);
            $request->validate([
                $field => "exists:\App\Models\\" . $models[$field] . ",id"
            ]);
        };
    }

    /**
     * Check if selected category Ids are valid and whether parent_id's match previous selected id
     * @param array $subCategories selected subCategory Ids
     * @return string $categoryPath 
     */
    public function checkCategoryHierarchy($subCategories)
    {
        if ($firstCategory = Category::whereHas("isTopCategory")->findOrFail(request("topCategory"))) {
            $parentId = $firstCategory->id;
            $categoryPath = "$parentId,";
            foreach ($subCategories as $subCategoryId) {
                $categoryPath .= $subCategoryId . ",";
                $category = Category::with("parent")->findOrFail($subCategoryId);
                if ($category->parent_id == $parentId) {
                    $parentId = $category->id;
                } else {
                    dd("SOMEONE TINKERED!");
                }
            }
        }
        return $categoryPath;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = AD::with("user", "imgs", "district", "color", "condition", "shoeSize", "waistSize", "size")->findOrFail($id);
        return view("ad.show", compact("ad"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        session()->forget("dynamicValidationRules");
        session()->put("dynamicValidationRules", []);

        $ad = Ad::where("user_id", Auth::id())->findOrFail($id);
        $topCategories = Category::whereHas("isTopCategory")->get();

        [$selectedCategoryIds, $selectedCategoryIdsNormalized] = $this->fetchSelectedCategories($ad);
        $selectedCategoriesOptions = $this->fetchOptionsForSelectedCategories($selectedCategoryIds);
        $lastCategoryId = $selectedCategoryIds[array_key_last($selectedCategoryIds)];

        // Fetch all Attributes for last selected category and put it in session to validate attributes
        $attributeTables = Category::findOrFail($lastCategoryId)->attributes;
        foreach ($attributeTables as $attributeTable) {
            $this->setValidationRules($attributeTable->table_name);
        }

        $attributesWithModels = $this->setModelsForCorrespondingAttributes();

        [$matchingAttributes, $matchingAttributesSelectedOptionIds] = $this->fetchAttributesWithSelectedOptions($attributesWithModels, $ad);

        $headers = ["color_id" => "Farbe", "size_id" => "Größe", "shoe_size_id" => "Schuhgröße", "waist_size_id" => "Bundweite", "condition_id" => "Zustand"];

        $borderColors = [
            "Mehrfarbig" => "mixed-colors-border",
            "Schwarz" => "black-border",
            "Braun" => "brown-border",
            "Blau" => "blue-border",
            "Türkis" => "turquoise-border",
            "Grün" => "green-border",
            "Gelb" => "yellow-border",
            "Orange" => "orange-border",
            "Rot" => "red-border",
            "Rosa" => "rose-border",
            "Violett" => "violet-border",
            "Grau" => "grey-border",
            "Silber" => "silver-border",
            "Gold" => "gold-border",
            "Beige" => "beige-border",
            "Weiß" => "white-border"
        ];


        $conditions = ClothingCondition::get();
        $districts = District::get();
        return view("ad.edit", compact("ad", "topCategories", "selectedCategoriesOptions", "selectedCategoryIdsNormalized", "matchingAttributes", "matchingAttributesSelectedOptionIds", "conditions", "districts", "headers", "borderColors"));
    }

    public function fetchSelectedCategories($ad)
    {
        $selectedCategoryIds = explode(",", $ad->category_path, -1);

        $selectedCategoryIdsNormalized = [];
        foreach ($selectedCategoryIds as $selectedCategoryId) {
            $selectedCategoryIdsNormalized[$selectedCategoryId] = $selectedCategoryId;
        }
        return [$selectedCategoryIds, $selectedCategoryIdsNormalized];
    }

    public function fetchOptionsForSelectedCategories($selectedCategoryIds)
    {
        $selectedCategoriesOptions = [];
        $lastCategoryId = null;
        foreach ($selectedCategoryIds as $key => $selectedCategoryId) {

            $key += 1; // For frontend SELECT Ids 
            if ($key == 1) {
                $topCategory = Category::with("children")->where("parent_id", null)->first();
                $selectedCategoriesOptions[$key] = $topCategory->children;
            } else {
                $subCategories = Category::where("parent_id", $lastCategoryId)->get();
                $selectedCategoriesOptions[$key] = $subCategories;
            }
            $lastCategoryId = $selectedCategoryId;
        }
        return $selectedCategoriesOptions;
    }

    public function setModelsForCorrespondingAttributes()
    {
        $allAttributeTables = Attribute::get();
        $attributes = [];
        foreach ($allAttributeTables as $attributeTable) {
            $basicAttributeName = substr($attributeTable->table_name, 0, strlen($attributeTable->table_name) - 1);
            $attributeName = $basicAttributeName . "_id";

            $basicModelName = explode("_", $basicAttributeName);
            $modelPath = "App\Models\\";
            foreach ($basicModelName as $basicModelName) {
                $modelPath .= ucfirst($basicModelName);
            }
            $attributes[$attributeName] = $modelPath;
        }

        return $attributes;
    }

    public function fetchAttributesWithSelectedOptions($attributesWithModels, $ad)
    {
        // All values for selected attribute
        $selectedAttributesOptions = [];
        // selected attributeIds for attributeOptions
        $selectedAttributeOptionIds = [];
        foreach ($attributesWithModels as $attribute => $model) {
            if (!isset($ad[$attribute])) continue;
            $selectedId = substr($attribute, 0, strrpos($attribute, "_") + 1) . $ad[$attribute];
            $selectedAttributeOptionIds[$selectedId] = $selectedId;
            $selectedAttributesOptions[$attribute] = $model::get();

            $containerName = substr($attribute, 0, strrpos($attribute, "_")) . "s";
            // TODO containername ist relativ egal, da geht's nur ums frontend
            $selectedAttributesOptions[$attribute]["containerName"] = $containerName;

            // set "attributeName" to models describing col-name (colorsTable -> "name", sizeTable -> "size")  
            $selectedAttributesOptions[$attribute]["attributeName"] = array_keys($selectedAttributesOptions[$attribute][0]->toArray())[1];
        }

        return [$selectedAttributesOptions, $selectedAttributeOptionIds];
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //TODO update details? und update 
        // TODO COPY PASTE AUFRÄUMEN!
        //TODO schauen, ob diese id auch bei einer deiner Anzeigen steht! 
        $ad = Ad::with("imgs")->where("user_id", Auth::id())->findOrFail($id);
        //! Always present rules 
        $request->validate([
            "name" => "required|min:3",
            "price" => "required|integer|min:0|max:99999",
            "price-comma" => "required|integer|min:0|max:99",
            "topCategory" => "required",
            "subCategories.*" => "required",
            "condition_id" => "required|exists:\App\Models\ClothingCondition,id",
            "district_id" => "required|exists:\App\Models\District,id",
        ]);

        // check which attributes have been set in session to validate
        $this->validateDynamicValidationRules($request);

        $categoryPath = $this->checkCategoryHierarchy($request->subCategories);

        // TODO nur die Daten aus der Session reinschreiben! Sonst kann man per Hand einfach Attribut Ids vergeben und sie werden eingespeichert!
        $sessionAtributes = session("dynamicValidationRules");
        $attributesToUpdate = [];
        foreach ($sessionAtributes as $sessionAtribute) {
            $attributesToUpdate[$sessionAtribute] = request($sessionAtribute);
        }
        $price = request("price") . "." . request("price-comma");
        $basicData = ["user_id" => Auth::id(), "category_path" => $categoryPath, "name" => request("name"), "condition_id" => request("condition_id"), "district_id" => request("district_id"), "price" => $price, "description" => request("description")];
        $dataForAd = array_merge($basicData, $attributesToUpdate);
        $ad->update($dataForAd);

        session()->forget("dynamicValidationRules");
        return redirect()->route("ad.editImgs", $id);
    }

    public function editImgs($id)
    {
        $ad = Ad::with("imgs")->where("user_id", Auth::id())->findOrFail($id);
        return view("ad.edit_imgs", compact("ad"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Fetch img for display in view
     * @param $img 
     * @return output Image
     */
    public function outputImg($img)
    {
        return $this->traitFileOutput($img);
    }
}
