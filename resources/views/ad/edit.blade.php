@extends("layouts.app")

@push("scripts")
<script src="{{asset("js/fetch_subcategories_for_add_ad.js")}}"></script>
<script src="{{asset("js/add_new_ad.js")}}"></script>
@endpush

@push("styles")
<link href="{{ asset('css/create.css') }}" rel="stylesheet">
@endpush

@section("content")
@if(session("success"))
<div class="alert alert-success">{{session("success")}}</div>
@elseif(session("error"))
<div class="alert alert-danger">{{session("error")}}</div>
@endif

<input type="hidden" name="fetchSubCategoriesRoute" value="{{route("ad.getSubcategories")}}"></input>
<input type="hidden" name="fetchAttributesRoute" value="{{route("ad.getAttributes")}}"></input>

<div class="my-5">
   <div>
      <a class="btn btn-primary" href="{{route("ad.index")}}">Zu Allen Inseraten</a>
      <a class="btn btn-warning" href="{{route("ad.editImgs",$ad->id)}}">Bilder überarbeiten</a>
   </div>
   <h3>Inserat bearbeiten</h3>
   <div class="new-ad-container">
      <form id="add-new-ad" action="{{route("ad.update",$ad->id)}}" method="post" enctype="multipart/form-data">
         @csrf
         @method("put")
         <div class="input-container">
            <div class="ad my-3">
               <div>
                  <label for="name">Titel</label>
                  <input type="text" id="ad-name" name="name" value="{{old("name") ?? $ad->name}}" autocomplete="off">
               </div>
               @error("name")
               <div class="alert alert-danger">{{$message}}</div>
               @enderror
               <div>
                  <label for="price">Preis</label>
                  <input type="number" id="ad-price" name="price" min="0" max="99999"
                     value="{{old("price") ?? substr($ad->price,0,strpos($ad->price,"."))}}" autocomplete="off">
                  @error("price")
                  <div class="alert alert-danger">{{$message}}</div>
                  @enderror
                  <span>,</span>
                  <input type="number" id="ad-price-comma" name="price-comma"
                     value="{{old("price-comma") ?? substr($ad->price,strpos($ad->price,".")+1)*1}}" autocomplete="off"
                     min="0" max="99">
                  @error("price-comma")
                  <div class="alert alert-danger">{{$message}}</div>
                  @enderror
               </div>
            </div>


            <div class="categories-container">
               <h3>Kategorien</h3>
               {{-- TODO mordsmäßig vereinfachen die Schweinerei! --}}
               <div class="d-flex flex-wrap ">
                  @foreach($selectedCategoriesOptions as $key => $select)
                  @if($key == 1)
                  <div class="category">
                     <select id="{{$key}}" name="topCategory" size="7">
                        @foreach($select as $option)
                        {{-- TODO aber auch auf OLD umbauen --}}
                        <option value="{{$option->id}}" @if(isset($selectedCategoryIdsNormalized[$option->id])) selected
                           @endif>{{$option->name}}</option>
                        @endforeach
                     </select>
                  </div>
                  @else
                  <div class="category d-flex align-items-center">
                     <i class='category-pointer fas fa-arrow-right'></i>
                     <select id="{{$key}}" name="subCategories[]" size="7">
                        @foreach($select as $option)
                        {{-- TODO aber auch auf OLD umbauen --}}
                        <option value="{{$option->id}}" @if(isset($selectedCategoryIdsNormalized[$option->id])) selected
                           @endif @if(count($selectedCategoriesOptions) == $key) bottom_category="1"
                           @endif>{{$option->name}}
                        </option>
                        @endforeach
                     </select>
                  </div>
                  @endif
                  @endforeach

                  @error("topCategory")
                  <div class="alert alert-danger">{{$message}}</div>
                  @enderror
                  @error("subCategories.*")
                  <div class="alert alert-danger">{{$message}}</div>
                  @enderror

               </div>
            </div>
            <div id="attributes-container">
               @foreach($matchingAttributes as $key => $attribute)
               <div id="attribute_{{$attribute["containerName"]}}">
                  <h3>{{$headers[$key]}}</h3>
                  <div class="{{$headers[$key] == "Farbe" ? "color-inputs" : ""}}">
                     @foreach($attribute as $id => $radioInput)
                     @if(is_string($radioInput)) @continue @endif
                     @php $id++ @endphp
                     <label class="{{$borderColors[$radioInput[$attribute["attributeName"]]] ?? ""}}"
                        for="{{substr($key,0,strrpos($key,"_"))}}_{{$id}}">
                        <input type="radio" id="{{substr($key,0,strrpos($key,"_"))}}_{{$id}}" name="{{$key}}"
                           @if(isset($matchingAttributesSelectedOptionIds[substr($key,0,strrpos($key,"_")) ."_" .$id]))
                           checked=true @endif value="{{$id}}">
                        {{$radioInput[$attribute["attributeName"]]}}
                     </label>
                     @endforeach
                  </div>
               </div>
               @endforeach
               <div id="attribute_conditions">
                  <h3>Zustand</h3>
                  <div>
                     @foreach($conditions as $key => $condition)
                     @php $key++ @endphp
                     <input type="radio" id="condition_{{$key}}" name="condition_id" value="{{$key}}"
                        @if($ad->condition_id
                     == $key) checked @endif>
                     <label for="condition_{{$key}}">{{$condition->condition}}</label>
                     @endforeach
                  </div>
               </div>
            </div>
            <div id='description'>
               <h3>Beschreibung</h3>
               <textarea name='description'>{{$ad->description}}</textarea>
            </div>
            <div id="districts_container">
               <h3>Verkaufsort</h3>
               <select id="districts" name="district_id">
                  <option value="">Bitte auswählen</option>
                  @foreach($districts as $key => $district)
                  @php $key++ @endphp
                  <option value="{{$key}}" @if($ad->district_id == $key) selected
                     @endif>{{$district->postal_code . ", ". $district->name}}</option>
                  @endforeach
               </select>
            </div>
         </div>
         <button id="add-ad-button" type="submit">Speichern und Weiter</button>

      </form>

   </div>
</div>
<script>
   const price = document.querySelector("#ad-price");
   price.onkeyup = function(e){
      if(this.value.length >=5){
         this.value = this.value.substring(0,5);
      }
   }

   const priceComma =document.querySelector("#ad-price-comma");
   priceComma.onkeyup = function(e){
      if(this.value.length >=2){
         this.value = this.value.substring(0,2);
      }
   }
   
</script>
@endsection