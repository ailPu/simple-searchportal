@extends("layouts.app")

@push("scripts")
{{-- <script src="{{asset("js/fetch_subcategories_for_add_ad.js")}}"></script>
<script src="{{asset("js/add_new_ad.js")}}"></script> --}}
@endpush

@push("styles")
<link href="{{ asset('css/show.css') }}" rel="stylesheet">
@endpush


@section("content")

<div>
   <a class="btn btn-primary" href="{{route("ad.index")}}">Zu Allen Inseraten</a>
   @if($ad->user_id == Auth::id())
   <a class="btn btn-warning" href="{{route("ad.edit",$ad->id)}}">Kategorien überarbeiten</a>
   <a class="btn btn-warning" href="{{route("ad.editImgs",$ad->id)}}">Bilder überarbeiten</a>
   @endif
</div>

<h3>{{$ad->name}}</h3>
<div class="row">
   <div class="col-sm-8 border">


      @if(count($ad->imgs))
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
         <ol class="carousel-indicators">
            @foreach($ad->imgs as $key => $img)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}" class="active"></li>
            @endforeach
         </ol>
         <div class="carousel-inner">
            @foreach($ad->imgs as $key => $img)
            <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
               <img class="d-block w-100" src="{{route("ad.outputImg","big_".$img->name)}}">
            </div>
            @endforeach
         </div>
         <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
         </a>
      </div>
      @else
      <img class="w-100" src=@if($ad->img) {{route("ad.outputImg","prev_".$ad->img->name )}} @else
      {{asset("img/photo-placeholder-icon-3.jpg")}} @endif width="150" height="100"
      style="object-fit:contain" alt="">
      @endif
      <div class="details">
         <h4>Detailinformationen</h4>
         <div class="row">
            @if($ad->color)
            <div class="color col">
               <p><strong>Farbe:</strong> <span>{{$ad->color->name}}</span></p>
            </div>
            @endif
            @if($ad->condition)
            <div class="condition col">
               <p><strong>Zustand:</strong><span>{{$ad->condition->condition}}</span></p>
            </div>
            @endif
            @if($ad->shoe_size)
            <div class="condition col">
               <p><strong>Schuhgröße:</strong><span>{{$ad->shoeSize->size}}</span></p>
            </div>
            @endif
            @if($ad->waist_size)
            <div class="condition col">
               <p><strong>Bundweite:</strong><span>{{$ad->waistSize->size}}</span></p>
            </div>
            @endif
            @if($ad->size)
            <div class="condition col">
               <p><strong>Größe:</strong><span>{{$ad->size->size}}</span></p>
            </div>
            @endif
         </div>
      </div>
      <div class="description">
         <h3>Beschreibung</h3>
         <p>{{$ad->description}}</p>
      </div>


   </div>


   <div class="col-sm-4 border">
      <h4>Kontakt</h4>
      <div class="row">
         <p class="col-sm-4">Name</p>
         <p class="col-sm">{{$ad->user->name}}</p>
      </div>
      <a href="" class="btn btn-primary">Nachricht senden</a>
      <div class="row my-3">
         <p class="col-sm-4"><strong>Adresse</strong></p>
         <p class="col-sm">{{$ad->district->postal_code}}, {{$ad->district->name}}</p>
      </div>
      <div class="price text-center py-2 bg-info rounded ">
         <span class="font-weight-bold">
            <span>€ </span>{{$ad->price}}
            <span class="d-block font-weight-normal">Verkaufspreis</span>
         </span>
      </div>

   </div>
</div>
@endsection