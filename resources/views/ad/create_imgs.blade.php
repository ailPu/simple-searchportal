@extends("layouts.app")

@section("content")
@if(session("success"))
<div class="alert alert-success">{{session("success")}}</div>
@elseif(session("error"))
<div class="alert alert-danger">{{session("error")}}</div>
@endif


<div class="my-5">
   <div>
      <a class="btn btn-primary" href="{{route("ad.index")}}">Zu Allen Inseraten</a>
      <a class="btn btn-warning" href="{{route("ad.edit",$ad->id)}}">Kategorien überarbeiten</a>
   </div>
   <h3>Bilder hinzufügen</h3>
   <p>Das oberste Bild ist dein Anzeigebild</p>
   <p>Ziehe die Bilder mit der Maus, wenn du die Reihenfolge ändern möchtest</p>
   <div class="img-container">
      <form id="add-new-ad" action="{{route("ad.storeImgs",$ad->id)}}" method="post" enctype="multipart/form-data">
         @csrf
         <input type="file" class="form-control-file d-none" id="upload-img" multiple>
         <button class="btn btn-primary submit">Speichern und Weiter</button>
      </form>
   </div>
   <button id="myButton" type="button" class="btn btn-success">+ Bilder hochladen</button>
   <ul id="images">
   </ul>
</div>

{{-- Animation --}}
<div class="overlay">
   <div class="lds-dual-ring"></div>
</div>

{{-- TODO ID anders einlesen, sprich umbauen zu normaler Route --}}
<input type="hidden" name="ad_id" value={{$ad->id}}>


{{-- <button id="mySubmitButton" type="button" style="border-radius: 5px; background-color: #fff; color: green;">Send
   Files</button> --}}

<div id="myFiles"></div>

<script>
   // TODO SCRIPT AUSLAGERN
   $(function(){
      // Reorder IDs in DOM
      function reorderIds(){
            $("#images li").each((idx,el) => $(el).attr("id",idx));
         }

      function dragImgs(){

         // remove all previously attached eventlisteners
         $("#images li").off();

         $("#images li").attr("draggable",true);

         reorderIds();

         var dragged = null;

         $("#images li").on("dragstart",function(e){
               // get native Event and Element
               e.originalEvent.dataTransfer.setDragImage($(e.target)[0], 150, 150);
               dragged = this;

               for(const li of $("#images li")){
                  if(li != this) $(li).addClass("hint")
               }
            })

         $("#images li").on("dragenter", function (ev) {
            if (this != dragged) { 
               this.classList.add("active"); 
            }
         });

         $("#images li").on("dragleave", function (e) {
            this.classList.remove("active");
         });

         // remove all added classes when dragging stops
         $("#images li").on("dragend", function () {
            for (const li of $("#images li")) {
               $(li).removeClass("hint");
               $(li).removeClass("active");
            }
         });

         // Allow custom dropping (fired every time an element gets dragged over a valid drop target)
         $("#images li").on("dragover", function(e){
            e.preventDefault();
         });

         $("#images li").on("drop",function dropHandler(e){
            e.preventDefault();
            // Fetch all images, because order changes after each drop
            var items = $("#images li");
            // If element that is getting dropped upon isn't the dragged one, continue
            if (this != dragged) {
               let oldPos = 0, droppedPos = 0;
               for (let it=0; it<items.length; it++) {
                  // oldPos = position in DOM before dragging (ex: first element (index=0))
                  if (dragged == items[it]) oldPos = it; 
                  // droppedPos = position of element that is dropped upon (ex: second element (index=1))
                  if (this == items[it]) droppedPos = it; 
               }
               // oldPos = 0, droppedPos = 1, this = li at droppedPos(index = 1), this.nextSibling = li(index = 2). 
               // firstLi gets inserted in front of the nextSibling of secondLi (= thirdLi)
               if (oldPos < droppedPos) this.parentNode.insertBefore(dragged, this.nextSibling);
               // dragger = 3, Ziehen auf 1, this = 1, also füge 3 vor 1 ein
               else this.parentNode.insertBefore(dragged, this);
            }
            // set img ids for database show order
            reorderIds();
         });
      };

      
      function readFile(file) {
         return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = res => {
            resolve(res.target.result);
            };
            reader.onerror = err => reject(err);

            reader.readAsDataURL(file);
         });
      }

      $("#myButton").on("click",() => $('#upload-img').trigger("click"));

      $("#upload-img").change(async() => {
         const files = document.querySelector('[type=file]').files;
         for(const file of files){
            $(".overlay").addClass("d-flex");
            const content = await readFile(file);
            var img = document.createElement("img");

            // wird gefeuert, sobald ich img.src setze (weiter unten)
            img.onload = function(){
               var canvas = document.createElement("canvas");
               var ctx = canvas.getContext("2d");
               ctx.drawImage(img, 0, 0);

               // Lege Größe fest TODO andere Bildschirme unterstützen
               const MAX_WIDTH = 300;
               const HEIGHT = 150;

               // Bildgrößen
               var width = img.width;
               var height = img.height;
               
               // Vergleiche Bildgrößen und passe sie, wenn nötig an MAX_WIDTH / HEIGHT an
               if (width > height) {
                  if (width > MAX_WIDTH) {
                     height *= MAX_WIDTH / width;
                     width = MAX_WIDTH;
                  }
               } else {
                  if (height > HEIGHT) {
                     width *= HEIGHT / height;
                     height = HEIGHT;
                  }
               }
               // Canasgrößen
               canvas.width = width;
               canvas.height = HEIGHT;
               var ctx = canvas.getContext("2d");
               
               // Zeichen Bild mit den runtergesetzten Dimensionen
               ctx.drawImage(img, 0, 0, width, height);
               
               // erstellt eine URL aus der übergebenen Datei, die ich dann bei der img.src einsetzen kann
               dataurl = canvas.toDataURL(file.type);
               
               const previewImages = document.querySelector("#images")
               const li = document.createElement("li");
               li.classList.add("d-flex");
               const previewImg = $(`<img class='file ml-auto' src=${dataurl}>`).data('fileData',file)
               const removeBtn = $("<button></button>").addClass("btn btn-danger ml-auto align-self-start").text("X");
               $(removeBtn).on("click",function(e){
                  e.preventDefault();
                  $(this).closest("li").remove();
                  reorderIds();
               })

               $(li).append(previewImg);
               $(li).append(removeBtn);
               $(previewImages).append($(li));
               dragImgs();
            }
            // durch das setzen der src feuert img.onload
            img.src = content;
         }   
         console.log("Preview Upload finished,cancel animation");
         $(".overlay").removeClass("d-flex");
      })
   })


   // Submit Form
   $('form .btn.submit').on("click",function(e) {
      $(".overlay").addClass("d-flex");
      e.preventDefault();
      const formData = new FormData();
      
      $("#images li img").each((idx,el)=>{
         console.log("submitted. El.data",$(el).data("fileData"));
         // Damit ich die Bilder im Trait ansprechen kann, brauchen sie alle eigene keys und kein Array
         formData.append(`image_${idx}`, $(el).data("fileData"));
      })

      // zum Auslesen des Formdataobjekts, ob es klappt 
      for(const keyValue of formData){
         console.log("Form key value pair",keyValue);
         const indexforDb = keyValue[0].substring(keyValue[0].lastIndexOf('_') + 1);
      }

      // return;
      
      // formData.set("images",$("#images p:first").data("fileData"))
      formData.append("ad_id",$("[name=ad_id]").val());
      console.log('Sending...');
      
      $.ajax({
         url: $("form#add-new-ad").attr("action"),
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         data: formData,
         type: 'POST',
         contentType:"application/x-www-form-urlencoded",
         success: function(data) { 
            console.log('SUCCESS !!!');
            console.log("DATA",data); 
            window.location.href = data.successRoute;
            // TODO Redirect to Next Step
            },
         error: function(data) { 
            console.log('ERROR !!!'); 
            // TODO Redirect 404
         },
         cache: false,
         processData: false,
         contentType: false
      });
   });




</script>


<style>
   /* TODO STYLE AUSLAGERN */
   .holder {
      height: 300px;
      width: 300px;
      border: 2px solid black;
   }

   /* Da beim drawImg alles in Proportionen passiert, muss ich nichts festlegen */
   img {
      /* max-width: 300px;
      max-height: 150px;
      min-width: 300px;
      height: 150px; */
      /* object-fit: contain; */
   }

   input[type="file"] {
      margin-top: 5px;
   }

   .heading {
      font-family: Montserrat;
      font-size: 45px;
      color: green;
   }

   .hint {
      background: yellow;
   }

   .active {
      background: green;
   }

   ul#images {
      list-style: none;
   }

   ul#images li {
      text-align: center;
      border: 1px solid black;
      padding: .5rem;
   }

   /* Necessary to prevent the removal of classes when dragging over img/parent  */
   img {
      pointer-events: none;
   }

   /* Overlay für Animation */

   /* TODO STYLE AUSLAGERN */

   .overlay {
      min-height: 100vh;
      background: rgba(0, 0, 0, .3);
      position: fixed;
      display: none;
      justify-content: center;
      align-items: center;
      top: 0;
      left: 0;
      width: 100%;
   }

   /* Animation */

   .lds-dual-ring {
      display: inline-block;
      width: 80px;
      height: 80px;
   }

   .lds-dual-ring:after {
      content: " ";
      display: block;
      width: 64px;
      height: 64px;
      margin: 8px;
      border-radius: 50%;
      border: 6px solid #fff;
      border-color: #fff transparent #fff transparent;
      animation: lds-dual-ring 1.2s linear infinite;
   }

   @keyframes lds-dual-ring {
      0% {
         transform: rotate(0deg);
      }

      100% {
         transform: rotate(360deg);
      }
   }
</style>
@endsection