@extends("layouts.app")

@push("scripts")
<script src="{{asset("js/fetch_subcategories_for_add_ad.js")}}"></script>
<script src="{{asset("js/add_new_ad.js")}}"></script>
@endpush

@push("styles")
<link href="{{ asset('css/create.css') }}" rel="stylesheet">
@endpush

@section("content")
@if(session("success"))
<div class="alert alert-success">{{session("success")}}</div>
@elseif(session("error"))
<div class="alert alert-danger">{{session("error")}}</div>
@endif

<input type="hidden" name="fetchSubCategoriesRoute" value="{{route("ad.getSubcategories")}}"></input>
<input type="hidden" name="fetchAttributesRoute" value="{{route("ad.getAttributes")}}"></input>

<div class="my-5">
   <a href="{{route("ad.index")}}">Zu Allen Inseraten</a>
   <h3>Neues Inserat</h3>
   <div class="new-ad-container">
      <form id="add-new-ad" action="{{route("ad.store")}}" method="post" enctype="multipart/form-data">
         @csrf
         <div class="input-container">
            <div class="ad my-3">
               <div>
                  <label for="name">Titel</label>
                  <input type="text" id="ad-name" name="name" value="{{old("name")}}" autocomplete="off">
               </div>
               @error("name")
               <div class="alert alert-danger">{{$message}}</div>
               @enderror
               <div>
                  <label for="price">Preis</label>
                  <input type="number" id="ad-price" name="price" min="0" max="99999" value="{{old("price") ?? 0}}"
                     autocomplete="off">
                  @error("price")
                  <div class="alert alert-danger">{{$message}}</div>
                  @enderror
                  <span>,</span>
                  <input type="number" id="ad-price-comma" name="price-comma" value="{{old("price-comma") ?? 00}}"
                     autocomplete="off" min="0" max="99">
                  @error("price-comma")
                  <div class="alert alert-danger">{{$message}}</div>
                  @enderror
               </div>
            </div>

            <div class="categories-container">
               <h3>Kategorien</h3>
               <div class="d-flex flex-wrap ">
                  <div class="category">
                     <select id="1" name="topCategory" size="7">
                        @foreach($topCategories as $topCategory)
                        <option value='{{$topCategory->id}}' name='{{$topCategory->name}}'> {{$topCategory->name}}
                        </option>
                        @endforeach
                     </select>
                  </div>
               </div>
            </div>
            @error("topCategory")
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
            @error("subCategories.*")
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
         </div>
         <button id="add-ad-button" type="submit">Speichern und Weiter</button>
      </form>

   </div>
</div>
<script>
   const price = document.querySelector("#ad-price");
   price.onkeyup = function(e){
      if(this.value.length >=5){
         this.value = this.value.substring(0,5);
      }
   }

   const priceComma =document.querySelector("#ad-price-comma");
   priceComma.onkeyup = function(e){
      if(this.value.length >=2){
         this.value = this.value.substring(0,2);
      }
   }
   
</script>
@endsection