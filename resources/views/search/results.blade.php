@extends("layouts.app")

@push("scripts")
<script src="{{asset("js/fetch_products_from_top_category.js")}}"></script>
<script src="{{asset("js/fetch_products_from_sub_category.js")}}"></script>
@endpush

@section("content")
<div>
   <a href="{{route("ad.create")}}">Neues Inserat aufgeben</a>
</div>

{!! $menuHtml !!}
<div class="d-flex y-5">
   <div class="align-self-center mr-2">
      <form id="search" action={{route("search.getResults")}} method="GET" autocomplete="off">
         <h4 class="text-center">Suche</h4>
         <div class="input-container">
            <input type="text" name="keyword" value='{{$_GET["keyword"] ?? ""}}'>
            <div class="results"></div>
         </div>
         @foreach($_GET as $key => $value)
         @if ($key == "keyword" || $key == "page") @continue @endif
         <input type="hidden" name={{$key}} value={{$value}}>
         @endforeach
         <div class="button-container text-center"><button type="submit">Suchen</button></div>
      </form>
   </div>
   <div class="align-self-center">
      <form action={{route("search.getResults")}} method="GET">
         @foreach($_GET as $key => $value)
         @if ($key == "keyword" || $key == "page") @continue @endif
         <input type="hidden" name={{$key}} value={{$value}}>
         @endforeach
         <button type="submit">Suchbegriff entfernen</button>
      </form>
   </div>
</div>


<div class="my-5">
   @if (!count($adsPaginated))
   @if (!isset($_GET["keyword"]) || $_GET["keyword"] == false)
   <h3> Es sind noch keine Einträge vorhanden </h3>
   @elseif(isset($_GET["keyword"]))
   <h3>Keine Einträge für '{{$_GET["keyword"]}}'</h3>
   @endif
   @else
   <h3 class="text-center">{{$adsPaginated->total()}} @if ($adsPaginated->total() == 1) Eintrag @else Einträge
      @endif
      @if(isset($_GET["keyword"]) && $_GET["keyword"])
      für '{{$_GET["keyword"]}}'@endif
   </h3>
   @endif

   {{$adsPaginated->appends(request()->input())->links('pagination::bootstrap-4')}}
   @foreach($adsPaginated as $ad)
   <div style="border:1px solid; text-align:center">
      <a href="{{route("ad.show",$ad->id)}}">
         <img src=@if($ad->img) {{route("ad.outputImg","prev_".$ad->img->name )}} @else
         {{asset("img/photo-placeholder-icon-3.jpg")}} @endif width="150" height="100"
         style="object-fit:contain" alt="">
         <p>{{$ad->name}}</p>
      </a>
   </div>
   @endforeach
</div>
@endsection