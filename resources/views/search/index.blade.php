@extends("layouts.app")


@push("styles")
<link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endpush

@push("scripts")
<script src="{{asset("js/fetch_products_from_top_category.js")}}"></script>
@endpush

@section("content")
<div>
   <a class="add-new-ad" data-check-login-status="{{route('user.check_login_status')}}"
      href="{{route("ad.create")}}">Neues Inserat aufgeben</a>
</div>
<ul>
   @foreach($root->children as $child)
   <li>
      <a
         href="{{route("search.getResults")}}?category_path={{$child->id}},&currCatId={{$child->id}}&currLevel=1&keyword=">{{$child->name}}</a>
   </li>
   @endforeach
</ul>
<h4 class="text-center">Wonach suchst du?</h4>
<div class="row justify-content-center">
   <div>
      <form id="search" action="{{route("search.getResults")}}" class="form-inline my-2 my-lg-0" method="GET"
         autocomplete="off">
         <input type="hidden" name="category_path" value="">
         <input type="hidden" name="currLevel" value="0">
         <input type="hidden" name="currCatId" value={{$root->id}}>
         <input class="form-control mr-sm-2" type="text" name="keyword" value="" placeholder="Säge...">
         <button type="submit" class="btn btn-primary">
            <i class="fas fa-search"></i>
         </button>
      </form>
      <div class="results"></div>
   </div>
</div>

@endsection