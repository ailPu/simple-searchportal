@extends("layouts.app")


@push("scripts")
<script src="{{asset("js/fetch_products_from_top_category.js")}}"></script>
@endpush

@section("content")

@if(session("success"))
<div class="alert alert-success"> {{session("success")}}</div>
@endif
<div>
   <a class="add-new-ad" href="{{route("ad.create")}}">Neues Inserat aufgeben</a>
</div>

<div class="my-5">
   @if (!count($adsPaginated))
   <h3> Du hast noch keine Anzeige aufgegeben </h3>
   @else
   <h3 class="text-center">Deine Anzeigen
   </h3>
   @endif

   {{$adsPaginated->links('pagination::bootstrap-4')}}
   @foreach($adsPaginated as $ad)
   <div style="border:1px solid; text-align:center">
      <a href="{{route("ad.edit",$ad->id)}}">
         {{-- TODO Auch die klassiche ANZEIGE Route dazugeben --}}
         {{-- <a href="{{route("ad.show",$ad->id)}}"> --}}
         <img src=@if($ad->img) {{route("ad.outputImg","prev_".$ad->img->name )}} @else
         {{asset("img/photo-placeholder-icon-3.jpg")}} @endif
         width="150"
         height="100" style="object-fit:contain" alt="">
         <p>{{$ad->name}}</p>
      </a>
   </div>
   @endforeach
</div>


@endsection