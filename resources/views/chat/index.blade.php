@extends('layouts.app')

@section('content')

<template>
   <a v-for="partner in partners" :href="partner.id" :key="partner.id" @click="activeFriend=partner.id"></a>
</template>

<div class="container">
   <div class="row">
      <div class="col-md-8 col-md-offset-2">
         <div class="panel panel-default">
            <div class="panel-heading">Chats</div>

            <div class="panel-body">
               <chat-messages :messages="messages"></chat-messages>
            </div>
            <div class="panel-footer">
               <chat-form v-on:messagesent="addMessage" :user="{{ Auth::user() }}"></chat-form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

{{-- ORI ohne Liste. Direkt Nachricht mit einer Person --}}
{{-- <div class="container">
   <div class="row">
      <div class="col-md-8 col-md-offset-2">
         <div class="panel panel-default">
            <div class="panel-heading">Chats</div>

            <div class="panel-body">
               <chat-messages :messages="messages"></chat-messages>
            </div>
            <div class="panel-footer">
               <chat-form v-on:messagesent="addMessage" :user="{{ Auth::user() }}"></chat-form>
</div>
</div>
</div>
</div>
</div>
@endsection --}}