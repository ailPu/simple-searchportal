if($(".add-new-ad").length >0){

   $(".add-new-ad").on("click",function(e){
      e.preventDefault();

      var addnewAdRoute = $(".add-new-ad").attr("href");
      $.ajax({
         // jQuery converts hyphens from Html to camelCase
         url: $(this).data("checkLoginStatus"),
         type: "get",
         success: function(resp){
            if(!resp){
               $(".modal-body > p:first").text("Um eine neue Anzeige aufzugeben musst du eingeloggt sein");
               $(".modal").modal("show");
            }
            else window.location.href = addnewAdRoute;
         }
      })
   })
}