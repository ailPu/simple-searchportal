/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


// window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('chat-messages', require('./components/ChatMessages.vue').default);
// Vue.component('chat-form', require('./components/ChatForm.vue').default);

// const app = new Vue({
//    el: '#app',

//    data: {
//       messages: [],
//       users :[],
//       user :[],
//    },

//    created() {
//       console.log("created");

//       this.fetchMessages();
//       this.fetchUsers();
//       Echo.private('chat')
//       .listen('MessageSent', (e) => {
//          this.messages.push({
//             message: e.message.message,
//             user: e.user
//          });
//       });
//    },

//    computed:{
//       partners(){
//          return this.users.filter((user)=> user.id !== this.user.id)
//       }
//    },

//    methods: {
//       fetchMessages(partnerId) {
//          console.log("fetching messages");
//          // TODO hardcoded userids entfernen
//          axios.get(`/messages/2`).then(response => {
//                // console.log("AFTERWARDS");
//             // axios.get(`/messages+${partnerId}`).then(response => {
//                this.messages = response.data;
//             });
//       },

//       fetchUsers() {
//             axios.get('/users').then(response => {
//                console.log("Fetching users",response);
//                // gegenwärtig festgelegt, dass alle User, nur nicht ich geholt werden soll
//                this.users = response.data.filter((user)=> user.id !== 1);
//                // this.users = response.data;
//             });
//       },

//       addMessage(message) {
//          // TODO hardcoded userids entfernen
//          axios.post('/messages/2', message).then(response => {
//             this.messages.push(response.data.message);
//             this.message = null;
//             console.log(response.data);
//             });
//       }
//    }
// });