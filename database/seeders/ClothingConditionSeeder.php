<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClothingConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clothing_conditions')->insert([
            [
                'condition' => "Neu",
            ],
            [
                'condition' => "Neuwertig",
            ],
            [
                'condition' => "Gebraucht",
            ],
            [
                'condition' => "Defekt",
            ],
        ]);
    }
}
