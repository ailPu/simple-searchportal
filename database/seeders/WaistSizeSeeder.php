<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WaistSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('waist_sizes')->insert([
            [
                'size' => "bis 28",
            ],
            [
                'size' => "29 / S",
            ],
            [
                'size' => "30 / S",
            ],
            [
                'size' => "31 / M",
            ],
            [
                'size' => "32 / M",
            ],
            [
                'size' => "33 / L",
            ],
            [
                'size' => "34 / L",
            ],
            [
                'size' => "35 / XL",
            ],
            [
                'size' => "36 / XL",
            ],
            [
                'size' => "37 / XL",
            ],
        ]);
    }
}
