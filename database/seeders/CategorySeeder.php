<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'parent_id' => NULL,
                'name' => "Alle Kategorien",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 1,
                'name' => "Mode / Accesoires",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 2,
                'name' => "Damenmode",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 3,
                'name' => "Accessoires",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 4,
                'name' => "Aufnäher / Buttons",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 4,
                'name' => "Gürtel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 4,
                'name' => "Haarschmuck",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 4,
                'name' => "Handschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 4,
                'name' => "Hauben",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 4,
                'name' => "Hüte",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 4,
                'name' => "Kappen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 4,
                'name' => "Regenschirme",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 4,
                'name' => "Schals / Tücher",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 4,
                'name' => "Sonnenbrillen / Etuis",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Arbeitsbekleidung",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 15,
                'name' => "Accesoires",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 15,
                'name' => "Arbeitshosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 15,
                'name' => "Arbeitsjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 15,
                'name' => "Arbeitsschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 15,
                'name' => "Handschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 15,
                'name' => "Helme / Schutzbrillen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 15,
                'name' => "Mäntel / Overalls",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 15,
                'name' => "Service / Gastronomie",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 15,
                'name' => "Uniformen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Bademode",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 25,
                'name' => "Badeanzüge",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 25,
                'name' => "Badeshorts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 25,
                'name' => "Bikinis",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Blazer / Anzüge",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 29,
                'name' => "Anzüge / Kostüme",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 29,
                'name' => "Blazer",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Hochzeitsmode",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 32,
                'name' => "Accessoires / Dekoration",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 32,
                'name' => "Anstecker / Schmuck",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 32,
                'name' => "Hochzeitskleider",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 32,
                'name' => "Jacken / Boleros",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 32,
                'name' => "Röcke",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 32,
                'name' => "Schleier / Kopfschmuck",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 32,
                'name' => "Schuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Hosen",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 40,
                'name' => "Jeans",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 40,
                'name' => "Jogginghosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 40,
                'name' => "Kurze Hosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 40,
                'name' => "Lederhosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 40,
                'name' => "Overalls",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 40,
                'name' => "Stoffhosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Jacken / Mäntel",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 47,
                'name' => "Capes / Ponchos",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 47,
                'name' => "Daunenjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 47,
                'name' => "Jeansjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 47,
                'name' => "Lederjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 47,
                'name' => "Leichte Jacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 47,
                'name' => "Mäntel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 47,
                'name' => "Regenjacken / Outdoorjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 47,
                'name' => "Winterjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Kleider",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 56,
                'name' => "Abend- / Ballkleider",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 56,
                'name' => "Kurze Kleider",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 56,
                'name' => "Lange Kleider",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Pullover / Westen",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 60,
                'name' => "Boleros",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 60,
                'name' => "Kapuzenpullover",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 60,
                'name' => "Strickpullover",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 60,
                'name' => "Strickwesten",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 60,
                'name' => "Sweatjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 60,
                'name' => "Sweatshirts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Röcke",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 67,
                'name' => "Jeansröcke",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 67,
                'name' => "Knielange Röcke",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 67,
                'name' => "Lange Röcke",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 67,
                'name' => "Miniröcke",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Schuhe",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 72,
                'name' => "Ballerinas",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 72,
                'name' => "Flip Flops",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 72,
                'name' => "Gummistiefel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 72,
                'name' => "Halbschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 72,
                'name' => "Hausschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 72,
                'name' => "Pumps / Highheels",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 72,
                'name' => "Sandalen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 72,
                'name' => "Sneakers",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 72,
                'name' => "Stiefel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 72,
                'name' => "Stiefeletten",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Shirts / Blusen",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 83,
                'name' => "Blusen / Hemden",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 83,
                'name' => "Langarm Shirts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 83,
                'name' => "Poloshirts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 83,
                'name' => "Tops",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 83,
                'name' => "Tops / Trägershirts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 83,
                'name' => "T-Shirts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Trachtenmode",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 90,
                'name' => "Accessoires",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 90,
                'name' => "Dirndl",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 90,
                'name' => "Hosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 90,
                'name' => "Jacken / Mäntel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 90,
                'name' => "Pullover / Westen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 90,
                'name' => "Röcke",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 90,
                'name' => "Schuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 90,
                'name' => "Shirts / Blusen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 90,
                'name' => "Trachtenkosstüme",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Umstandsmode",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 100,
                'name' => "Bauchbänder",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 100,
                'name' => "Hosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 100,
                'name' => "Jacken / Mäntel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 100,
                'name' => "Kleider",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 100,
                'name' => "Pullover / Westen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 100,
                'name' => "Röcke",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 100,
                'name' => "Shirts / Blusen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 100,
                'name' => "Still-BHs",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 100,
                'name' => "Wäsche",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 3,
                'name' => "Wäsche",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 110,
                'name' => "Bademäntel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 110,
                'name' => "BHs",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 110,
                'name' => "Bodies / Korsagen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 110,
                'name' => "Leggings",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 110,
                'name' => "Pyjamas / Nachthemden",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 110,
                'name' => "Slips / Höschen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 110,
                'name' => "Socken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 110,
                'name' => "Strümpfe / Strumpfhosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 110,
                'name' => "Unterhemden",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 110,
                'name' => "Unterwäsche-Sets",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 2,
                'name' => "Herrenmode",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 121,
                'name' => "Accesoires",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 122,
                'name' => "Aufnäher / Buttons",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 122,
                'name' => "Gürtel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 122,
                'name' => "Handschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 122,
                'name' => "Hauben",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 122,
                'name' => "Hüte",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 122,
                'name' => "Kappen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 122,
                'name' => "Krawatten / Fliegen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 122,
                'name' => "Regenschirme",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 122,
                'name' => "Schals / Tücher",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 122,
                'name' => "Sonnenbrillen / Etuis",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Anzüge",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 133,
                'name' => "Anzüge",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 133,
                'name' => "Anzugswesten",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 133,
                'name' => "Krawatten / Fliegen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 133,
                'name' => "Sakkos",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Arbeitsbekleidung",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 138,
                'name' => "Accesoires",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 138,
                'name' => "Arbeitshosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 138,
                'name' => "Arbeitsjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 138,
                'name' => "Arbeitsschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 138,
                'name' => "Handschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 138,
                'name' => "Helme / Schutzbrillen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 138,
                'name' => "Mäntel / Overalls",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 138,
                'name' => "Service / Gastronomie",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 138,
                'name' => "Uniformen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Bademode",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 148,
                'name' => "Badehosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 148,
                'name' => "Badeshorts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Hosen",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 151,
                'name' => "Jeans",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 151,
                'name' => "Jogginghosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 151,
                'name' => "Kurze Hosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 151,
                'name' => "Lederhosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 151,
                'name' => "Overalls",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 151,
                'name' => "Stoffhosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Jacken / Mäntel",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 158,
                'name' => "Daunenjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 158,
                'name' => "Jeansjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 158,
                'name' => "Lederjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 158,
                'name' => "Leichte Jacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 158,
                'name' => "Mäntel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 158,
                'name' => "Regenjacken / Outdoorjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 158,
                'name' => "Winterjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Pullover / Westen",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 166,
                'name' => "Kapuzenpullover",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 166,
                'name' => "Strickpullover",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 166,
                'name' => "Strickwesten",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 166,
                'name' => "Sweatjacken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 166,
                'name' => "Sweatshirts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Schuhe",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 172,
                'name' => "Flip Flops",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 172,
                'name' => "Gummistiefel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 172,
                'name' => "Halbschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 172,
                'name' => "Hausschuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 172,
                'name' => "Sandalen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 172,
                'name' => "Sneakers",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 172,
                'name' => "Stiefel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 172,
                'name' => "Stiefeletten",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Shirts / Hemden",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 181,
                'name' => "Hemden",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 181,
                'name' => "Langarm Shirts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 181,
                'name' => "Poloshirts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 181,
                'name' => "Tanktops",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 181,
                'name' => "T-Shirts",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 181,
                'name' => "Trachtenmode",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Accessoires",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 188,
                'name' => "Hosen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 188,
                'name' => "Jacken / Mäntel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 188,
                'name' => "Pullover / Westen",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 188,
                'name' => "Schuhe",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 188,
                'name' => "Shirts / Hemden",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 188,
                'name' => "Trachtenanzüge",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 121,
                'name' => "Wäsche",
                'bottom_category' => 0,
            ],
            [
                'parent_id' => 195,
                'name' => "Bademäntel",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 195,
                'name' => "Pyjamas",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 195,
                'name' => "Slips / Short",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 195,
                'name' => "Socken",
                'bottom_category' => 1,
            ],
            [
                'parent_id' => 195,
                'name' => "Unterhemden",
                'bottom_category' => 1,
            ],
        ]);
    }
}
