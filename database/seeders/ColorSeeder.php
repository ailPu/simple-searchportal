<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            [
                'name' => "Mehrfarbig",
            ],
            [
                'name' => "Schwarz",
            ],
            [
                'name' => "Braun",
            ],
            [
                'name' => "Blau",
            ],
            [
                'name' => "Türkis",
            ],
            [
                'name' => "Grün",
            ],
            [
                'name' => "Gelb",
            ],
            [
                'name' => "Orange",
            ],
            [
                'name' => "Rot",
            ],
            [
                'name' => "Rosa",
            ],
            [
                'name' => "Violett",
            ],
            [
                'name' => "Grau",
            ],
            [
                'name' => "Silber",
            ],
            [
                'name' => "Gold",
            ],
            [
                'name' => "Beige",
            ],
            [
                'name' => "Weiß",
            ],
        ]);
    }
}
