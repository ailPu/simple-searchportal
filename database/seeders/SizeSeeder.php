<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->insert([
            [
                'size' => "bis 34",
            ],
            [
                'size' => "36 / S",
            ],
            [
                'size' => "38 / S",
            ],
            [
                'size' => "40 / M",
            ],
            [
                'size' => "42 / M",
            ],
            [
                'size' => "44 / L",
            ],
            [
                'size' => "46 / L",
            ],
            [
                'size' => "48 / XL",
            ],
            [
                'size' => "ab 50",
            ],
        ]);
    }
}
