<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            [
                'postal_code' => 1010,
                "name" => "Innere Stadt",
            ],
            [
                'postal_code' => 1020,
                "name" => "Leopoldstadt"
            ],
            [
                'postal_code' => 1030,
                "name" => "Landstraße"
            ],
            [
                'postal_code' => 1040,
                "name" => "Wieden"
            ],
            [
                'postal_code' => 1050,
                "name" => "Margareten"
            ],
            [
                'postal_code' => 1060,
                "name" => "Mariahilf"
            ],
            [
                'postal_code' => 1070,
                "name" => "Neubau"
            ],
            [
                'postal_code' => 1080,
                "name" => "Josefstadt"
            ],
            [
                'postal_code' => 1090,
                "name" => "Alsergrund"
            ],
            [
                'postal_code' => 1100,
                "name" => "Favoriten"
            ],
            [
                'postal_code' => 1110,
                "name" => "Simmering"
            ],
            [
                'postal_code' => 1120,
                "name" => "Meidling"
            ],
            [
                'postal_code' => 1130,
                "name" => "Hietzing"
            ],
            [
                'postal_code' => 1140,
                "name" => "Penzing"
            ],
            [
                'postal_code' => 1150,
                "name" => "Rudolfsheim Fünfhaus"
            ],
            [
                'postal_code' => 1160,
                "name" => "Ottakring"
            ],
            [
                'postal_code' => 1170,
                "name" => "Hernals"
            ],
            [
                'postal_code' => 1180,
                "name" => "Währing"
            ],
            [
                'postal_code' => 1190,
                "name" => "Döbling"
            ],
            [
                'postal_code' => 1200,
                "name" => "Brigittenau"
            ],
            [
                'postal_code' => 1210,
                "name" => "Floridsdorf"
            ],
            [
                'postal_code' => 1220,
                "name" => "Donaustadt"
            ],
            [
                'postal_code' => 1230,
                "name" => "Liesing"
            ],
        ]);
    }
}
