<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(ClothingConditionSeeder::class);
        $this->call(ShoeSizeSeeder::class);
        $this->call(SizeSeeder::class);
        $this->call(WaistSizeSeeder::class);
        $this->call(AttributeSeeder::class);
        $this->call(CategoryAttributeSeeder::class);
        $this->call(DistrictSeeder::class);
    }
}
