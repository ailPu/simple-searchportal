<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories_attributes')->insert([
            // Damenmode > Accessoires
            [
                "category_id" => 5,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 6,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 7,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 8,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 9,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 10,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 11,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 12,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 13,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 14,
                "attribute_id" => 1,
            ],
            // Damenmode > Arbeitsbekleidung
            [
                "category_id" => 16,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 17,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 17,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 18,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 18,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 19,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 19,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 20,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 22,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 22,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 23,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 23,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 24,
                "attribute_id" => 2,
            ],
            // Damen > Bademode

            [
                "category_id" => 26,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 26,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 27,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 27,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 28,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 28,
                "attribute_id" => 2,
            ],
            // Damenmode > Blazer / Anzüge
            [
                "category_id" => 30,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 30,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 31,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 31,
                "attribute_id" => 2,
            ],
            // Damenmode > Hochzeitsmode
            [
                "category_id" => 33,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 35,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 35,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 36,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 36,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 37,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 37,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 38,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 39,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 39,
                "attribute_id" => 3,
            ],
            // Damenmode > Hosen
            [
                "category_id" => 41,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 41,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 42,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 42,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 43,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 43,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 44,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 44,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 45,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 45,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 46,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 46,
                "attribute_id" => 4,
            ],
            // Damenmode > Jacken / Mäntel
            [
                "category_id" => 48,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 48,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 49,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 49,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 50,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 50,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 51,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 51,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 52,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 52,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 53,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 53,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 54,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 54,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 55,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 55,
                "attribute_id" => 2,
            ],
            // Damenmode > Kleider
            [
                "category_id" => 57,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 57,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 58,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 58,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 59,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 59,
                "attribute_id" => 2,
            ],
            // Damenmode > Pullover / Westen
            [
                "category_id" => 61,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 61,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 62,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 62,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 63,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 63,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 64,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 64,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 65,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 65,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 66,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 66,
                "attribute_id" => 2,
            ],
            // Damenmode > Röcke
            [
                "category_id" => 68,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 68,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 69,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 69,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 70,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 70,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 71,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 71,
                "attribute_id" => 2,
            ],
            // Damenmode > Schuhe
            [
                "category_id" => 73,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 73,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 74,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 74,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 75,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 75,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 76,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 76,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 77,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 77,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 78,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 78,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 79,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 79,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 80,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 80,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 81,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 81,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 82,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 82,
                "attribute_id" => 3,
            ],
            // Damenmode > Shirts / Blusen
            [
                "category_id" => 84,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 84,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 85,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 85,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 86,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 86,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 87,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 87,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 88,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 88,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 89,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 89,
                "attribute_id" => 2,
            ],
            // Damenbekleidung > Trachtenmode
            [
                "category_id" => 91,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 92,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 92,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 93,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 93,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 94,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 94,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 95,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 95,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 96,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 96,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 97,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 97,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 98,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 98,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 99,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 99,
                "attribute_id" => 2,
            ],
            // Damenmode > Umstandsmode
            [
                "category_id" => 101,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 102,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 102,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 103,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 103,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 104,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 104,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 105,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 105,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 106,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 106,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 107,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 107,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 108,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 109,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 109,
                "attribute_id" => 2,
            ],
            // Damenmode > Wäsche
            [
                "category_id" => 111,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 111,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 112,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 113,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 113,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 114,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 114,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 115,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 115,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 116,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 116,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 117,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 118,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 119,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 119,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 120,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 120,
                "attribute_id" => 2,
            ],
            // Herrenmode > Accessoires
            [
                "category_id" => 123,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 124,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 125,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 126,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 127,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 128,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 129,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 130,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 131,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 132,
                "attribute_id" => 1,
            ],
            // Herrenmode > Anzüge
            [
                "category_id" => 134,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 134,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 135,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 135,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 136,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 137,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 137,
                "attribute_id" => 2,
            ],
            // Herrenmode > Arbeitsbekleidung
            [
                "category_id" => 139,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 140,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 140,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 141,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 141,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 142,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 142,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 143,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 145,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 145,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 146,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 146,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 147,
                "attribute_id" => 2,
            ],
            // Herrenmode > Bademode
            [
                "category_id" => 149,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 149,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 150,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 150,
                "attribute_id" => 2,
            ],
            // Herrenmode > Hosen
            [
                "category_id" => 152,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 152,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 153,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 153,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 154,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 154,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 155,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 155,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 156,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 156,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 157,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 157,
                "attribute_id" => 4,
            ],
            // Herrenmode > Jacken / Mäntel
            [
                "category_id" => 159,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 159,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 160,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 160,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 161,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 161,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 162,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 162,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 163,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 163,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 164,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 164,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 165,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 165,
                "attribute_id" => 2,
            ],
            // Herrenmode > Pullover / Westen
            [
                "category_id" => 167,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 167,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 168,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 168,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 169,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 169,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 170,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 170,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 171,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 171,
                "attribute_id" => 2,
            ],
            // Herrenmode > Schuhe
            [
                "category_id" => 173,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 173,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 174,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 174,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 175,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 175,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 176,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 176,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 177,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 177,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 178,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 178,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 179,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 179,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 180,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 180,
                "attribute_id" => 3,
            ],
            // Herrenmode > Shirts / Hemden
            [
                "category_id" => 182,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 182,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 183,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 183,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 184,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 184,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 185,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 185,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 186,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 186,
                "attribute_id" => 2,
            ],
            // Herrenmode > Trachtenmode
            [
                "category_id" => 188,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 189,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 189,
                "attribute_id" => 4,
            ],
            [
                "category_id" => 190,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 190,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 191,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 191,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 192,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 192,
                "attribute_id" => 3,
            ],
            [
                "category_id" => 193,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 193,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 194,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 194,
                "attribute_id" => 2,
            ],
            // Herrenmode > Wäsche
            [
                "category_id" => 196,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 196,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 197,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 197,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 198,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 198,
                "attribute_id" => 2,
            ],
            [
                "category_id" => 199,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 200,
                "attribute_id" => 1,
            ],
            [
                "category_id" => 200,
                "attribute_id" => 2,
            ],
        ]);
    }
}
