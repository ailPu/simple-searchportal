<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attributes')->insert([
            [
                "table_name" => "colors",
            ],
            [
                "table_name" => "sizes",
            ],
            [
                "table_name" => "shoe_sizes",
            ],
            [
                "table_name" => "waist_sizes",
            ],
        ]);
    }
}
