<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShoeSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shoe_sizes')->insert([
            [
                'size' => "bis 36",
            ],
            [
                'size' => "37",
            ],
            [
                'size' => "38",
            ],
            [
                'size' => "39",
            ],
            [
                'size' => "40",
            ],
            [
                'size' => "41",
            ],
            [
                'size' => "ab 42",
            ],
        ]);
    }
}
