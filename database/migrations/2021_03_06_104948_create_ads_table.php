<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("district_id")->nullable();
            $table->unsignedBigInteger("condition_id");
            $table->unsignedBigInteger("color_id")->nullable();
            $table->unsignedBigInteger("size_id")->nullable();
            $table->unsignedBigInteger("shoe_size_id")->nullable();
            $table->unsignedBigInteger("waist_size_id")->nullable();
            $table->string("category_path", 200);
            $table->string("name", 100);
            $table->text("description")->nullable();
            $table->decimal("price", 7, 2)->default(0);
            $table->boolean("released")->default(0);
            $table->boolean("sold")->default(0);
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("restrict")->onUpdate("cascade");
            $table->foreign("district_id")->references("id")->on("districts")->onDelete("restrict")->onUpdate("cascade");
            $table->foreign("condition_id")->references("id")->on("clothing_conditions")->onDelete("restrict")->onUpdate("cascade");
            $table->foreign("color_id")->references("id")->on("colors")->onDelete("restrict")->onUpdate("cascade");
            $table->foreign("size_id")->references("id")->on("sizes")->onDelete("restrict")->onUpdate("cascade");
            $table->foreign("shoe_size_id")->references("id")->on("shoe_sizes")->onDelete("restrict")->onUpdate("cascade");
            $table->foreign("waist_size_id")->references("id")->on("waist_sizes")->onDelete("restrict")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
