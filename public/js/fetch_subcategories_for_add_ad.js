setEventListenerToAllSelects();

function setEventListenerToAllSelects() {
	// Remove all previous select-eventlisteners to prevent creating multiple on the same
	$(".categories-container select").off();

	// Set eventlisteners to all selects
	$("select:not(#districts)").on("change", debouncer((e) => {
		const _this = e.target;
		// remove every select after the one that has changed
		console.log("CHANGED",_this);
		$(_this).closest(".category").nextAll().remove();

		// If attributes are already in dom and user changes category (different attributes have to be fetched and old have to be removed)
		$(".categories-container").nextUntil("button[type=submit]").remove();

		// if user has selected option which is a bottomCategory, fetch attributes for that category
		if($("option:selected",_this).attr("bottom_category") == 1){
			fetchAttributes();
			return;
		}
		
		const fetchRoute = $("[name=fetchSubCategoriesRoute]").val();
		$.ajax({
			url: fetchRoute,
			method: "GET",
			data: {
				prevCategoryId: $(_this).val(),
			},
			dataType: "json",
			success: function (results) {
				const currentSelectCount = $("form#add-new-ad select").length;

				let subSelectsContainer = $("<div class='category d-flex align-items-center'><i class='category-pointer fas fa-arrow-right'></i>");
				let subSelects = $(
					`<select id="${currentSelectCount + 1}" name="subCategories[]" size="7">`
				);
				results.forEach((result) => {
					subSelects.append(
						`<option value="${result.id}" name="${result.name}" bottom_category=${result.bottom_category}>${result.name}</option>`
					);
				});
				subSelects.append("</select>");
				$(subSelectsContainer).append(subSelects);
				$(subSelectsContainer).insertAfter($(".categories-container .category").last());
				// subSelects.before(`<i class="category-pointer fas fa-arrow-right"></i>`);

				setEventListenerToAllSelects();
			},
		});
	}));
}

// TODO Datei teilen!

function fetchAttributes(){

	const bottomCategoryId = $(".categories-container select").last().val();
	console.log(bottomCategoryId);
	const fetchRouteForAttributes = $("[name=fetchAttributesRoute]").val();
	console.log(fetchRouteForAttributes);
	$.ajax({
		url: fetchRouteForAttributes,
		method: "GET",
		data: {
			categoryId: bottomCategoryId,
		},
		dataType: "json",
		success: function (results) {

			let attributes = $("<div id='attributes-container'></div>");
			for(const attributeCategory in results.attributes){

				const category = $(`<div id='attribute_${attributeCategory}'><h3>${results.headers[attributeCategory]}</h3></div>`);
				$(attributes).append(category);

				const attributesContainer = $(`<div class=${attributeCategory == "colors" ? 'color-inputs' : ''}>`);
				results.attributes[attributeCategory].forEach((attribute,index) =>{
					const attributeName = Object.keys(attribute)[1];

					// <label label for='${attributeCategory.substring(0,attributeCategory.length-1) + "_" + attribute.id}'>
					// 	<input id='${attributeCategory.substring(0,attributeCategory.length-1) + "_" + attribute.id}' name='${attributeCategory.substring(0,attributeCategory.length-1)}_id' value='${attribute.id}'>
					// 		<img class="img-normal" src="https://cache.willhaben.at/static/categorytree/image/3200_mehrfarbig_normal_d8b40492c0dc93fb8db031bc7e9ba29f78650e84.svg"> <img class=>${attribute[attributeName]}</span></label>

					// if(attributeCategory == "colors"){
						const colors = {
							Mehrfarbig: "mixed-colors-border",
							Schwarz: "black-border",
							Braun: "brown-border",
							Blau: "blue-border",
							Türkis: "turquoise-border",
							Grün: "green-border",
							Gelb: "yellow-border",
							Orange: "orange-border",
							Rot: "red-border",
							Rosa: "rose-border",
							Violett: "violet-border",
							Grau: "grey-border",
							Silber: "silver-border",
							Gold: "gold-border",
							Beige: "beige-border",
							Weiß: "white-border",
						}
						$(attributesContainer).append(`
						<label class="${colors[attribute[attributeName]] ?? ""}" for=${attributeCategory.substring(0,attributeCategory.length-1) + "_" + attribute.id}>
						<input type='radio' id='${attributeCategory.substring(0,attributeCategory.length-1) + "_" + attribute.id}' name='${attributeCategory.substring(0,attributeCategory.length-1)}_id' value='${attribute.id}'>
							${attribute[attributeName]}</span></label>
						`);
						$(category).append($(attributesContainer));
				});
			};

			
			$("form .input-container").append(attributes);
			$("form .input-container").append($("<div id='description'><h3>Beschreibung</h3><textarea name='description'>Beschreibe deine Anzeige..</textarea></div>"));
			
			const location = $("<div id='districts_container'></div>")
			$(location).append("<h3>Verkaufsort</h3>");

			let districtSelectBox = $(
				`<select id='districts' name="district_id"><option value="">Bitte auswählen</option>`
			);
			results.districts.forEach((result) => {
				districtSelectBox.append(
					`<option value="${result.id}" name="${result.name}">${result.postal_code + ", " + result.name}</option>`
				);
			});
			districtSelectBox.append("</select>");
			$(location).append(districtSelectBox);

			$("form .input-container").append(location);	
		},
	});
};

	function debouncer(fn,timeout=300) {
		var timer;
		// arg (or more parameters) is necessary to pass it inside setTimeout, since setTimeout's this = window, and I need the event object inside onclick function
		return function(arg){
			clearTimeout(timer);
			timer = setTimeout(()=>{
				// Wenn ich APPLY verwende, dann kommen die Argumente als Array rüber
				// TODO apply braucht es hier eigentlich gar nicht, weil ich keinen Kontext brauche!
				fn.apply(null,[arg]);

				// Wenn ich CALL verwende, dann müssen die Argumente per KOMMA getrennt werden
				// fn.call(context,arg);
			}, timeout);
		}
	}