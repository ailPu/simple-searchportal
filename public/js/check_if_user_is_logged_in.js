/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/check_if_user_is_logged_in.js":
/*!****************************************************!*\
  !*** ./resources/js/check_if_user_is_logged_in.js ***!
  \****************************************************/
/***/ (() => {

eval("if ($(\".add-new-ad\").length > 0) {\n  $(\".add-new-ad\").on(\"click\", function (e) {\n    e.preventDefault();\n    var addnewAdRoute = $(\".add-new-ad\").attr(\"href\");\n    $.ajax({\n      // jQuery converts hyphens from Html to camelCase\n      url: $(this).data(\"checkLoginStatus\"),\n      type: \"get\",\n      success: function success(resp) {\n        if (!resp) {\n          $(\".modal-body > p:first\").text(\"Um eine neue Anzeige aufzugeben musst du eingeloggt sein\");\n          $(\".modal\").modal(\"show\");\n        } else window.location.href = addnewAdRoute;\n      }\n    });\n  });\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvY2hlY2tfaWZfdXNlcl9pc19sb2dnZWRfaW4uanM/OWY4ZSJdLCJuYW1lcyI6WyIkIiwibGVuZ3RoIiwib24iLCJlIiwicHJldmVudERlZmF1bHQiLCJhZGRuZXdBZFJvdXRlIiwiYXR0ciIsImFqYXgiLCJ1cmwiLCJkYXRhIiwidHlwZSIsInN1Y2Nlc3MiLCJyZXNwIiwidGV4dCIsIm1vZGFsIiwid2luZG93IiwibG9jYXRpb24iLCJocmVmIl0sIm1hcHBpbmdzIjoiQUFBQSxJQUFHQSxDQUFDLENBQUMsYUFBRCxDQUFELENBQWlCQyxNQUFqQixHQUF5QixDQUE1QixFQUE4QjtBQUUzQkQsRUFBQUEsQ0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQkUsRUFBakIsQ0FBb0IsT0FBcEIsRUFBNEIsVUFBU0MsQ0FBVCxFQUFXO0FBQ3BDQSxJQUFBQSxDQUFDLENBQUNDLGNBQUY7QUFFQSxRQUFJQyxhQUFhLEdBQUdMLENBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJNLElBQWpCLENBQXNCLE1BQXRCLENBQXBCO0FBQ0FOLElBQUFBLENBQUMsQ0FBQ08sSUFBRixDQUFPO0FBQ0o7QUFDQUMsTUFBQUEsR0FBRyxFQUFFUixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFTLElBQVIsQ0FBYSxrQkFBYixDQUZEO0FBR0pDLE1BQUFBLElBQUksRUFBRSxLQUhGO0FBSUpDLE1BQUFBLE9BQU8sRUFBRSxpQkFBU0MsSUFBVCxFQUFjO0FBQ3BCLFlBQUcsQ0FBQ0EsSUFBSixFQUFTO0FBQ05aLFVBQUFBLENBQUMsQ0FBQyx1QkFBRCxDQUFELENBQTJCYSxJQUEzQixDQUFnQywwREFBaEM7QUFDQWIsVUFBQUEsQ0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZYyxLQUFaLENBQWtCLE1BQWxCO0FBQ0YsU0FIRCxNQUlLQyxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCWixhQUF2QjtBQUNQO0FBVkcsS0FBUDtBQVlGLEdBaEJEO0FBaUJGIiwic291cmNlc0NvbnRlbnQiOlsiaWYoJChcIi5hZGQtbmV3LWFkXCIpLmxlbmd0aCA+MCl7XHJcblxyXG4gICAkKFwiLmFkZC1uZXctYWRcIikub24oXCJjbGlja1wiLGZ1bmN0aW9uKGUpe1xyXG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICB2YXIgYWRkbmV3QWRSb3V0ZSA9ICQoXCIuYWRkLW5ldy1hZFwiKS5hdHRyKFwiaHJlZlwiKTtcclxuICAgICAgJC5hamF4KHtcclxuICAgICAgICAgLy8galF1ZXJ5IGNvbnZlcnRzIGh5cGhlbnMgZnJvbSBIdG1sIHRvIGNhbWVsQ2FzZVxyXG4gICAgICAgICB1cmw6ICQodGhpcykuZGF0YShcImNoZWNrTG9naW5TdGF0dXNcIiksXHJcbiAgICAgICAgIHR5cGU6IFwiZ2V0XCIsXHJcbiAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3Ape1xyXG4gICAgICAgICAgICBpZighcmVzcCl7XHJcbiAgICAgICAgICAgICAgICQoXCIubW9kYWwtYm9keSA+IHA6Zmlyc3RcIikudGV4dChcIlVtIGVpbmUgbmV1ZSBBbnplaWdlIGF1Znp1Z2ViZW4gbXVzc3QgZHUgZWluZ2Vsb2dndCBzZWluXCIpO1xyXG4gICAgICAgICAgICAgICAkKFwiLm1vZGFsXCIpLm1vZGFsKFwic2hvd1wiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gYWRkbmV3QWRSb3V0ZTtcclxuICAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICB9KVxyXG59Il0sImZpbGUiOiIuL3Jlc291cmNlcy9qcy9jaGVja19pZl91c2VyX2lzX2xvZ2dlZF9pbi5qcy5qcyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/check_if_user_is_logged_in.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/check_if_user_is_logged_in.js"]();
/******/ 	
/******/ })()
;