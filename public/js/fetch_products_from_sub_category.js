// Get GET_Parameters
const queryStringAtSubCategory = window.location.search;
const urlParamsAtSubCategory = new URLSearchParams(queryStringAtSubCategory);
const currentcategory_pathAtSubCategory = urlParamsAtSubCategory.get("category_path");
const currentCatId = urlParamsAtSubCategory.get("currCatId"); 
const currLevel = urlParamsAtSubCategory.get("currLevel");

if(currentcategory_pathAtSubCategory){
   $("form#search input[name=keyword]").on("keyup", function(e) {
      if ($(this).val().length < 3) {
         // Empty results if input below 3 
         $(".results").empty();
         return;
      }
      const searchRoute = $("form#search").attr("action");
      const keyword = $(this).val();
      $.ajax({
         url: searchRoute,
         method: "GET",
         data: {
            "keyword": $(this).val(),
            "category_path": currentcategory_pathAtSubCategory,
            "currLevel": currLevel,
            "atSubLevel": true,
         },
         dataType: "json",
         success: function(results) {
            console.log(results);
            // Remove old results
            $(".results").empty();
            if(!results.length){
               console.log("Keine Treffer");
               return;
            }
            let searchResults = $("<ul></ul>");
            results.forEach((result) => {
               let resultHtml = `<li><a href=${searchRoute}?category_path=${result.category_path}&currCatId=${result.deepest_category_id}&amp;currLevel=${result.currLevel}&keyword=${keyword}>${result.cat_count} Treffer <span>in ${result.second_highest_category_name} > ${result.one_before_deepest_category_name} > ${result.deepest_category_name}</span></a></li>`;
               searchResults.append(resultHtml); 
            });

            $(".results").html(searchResults)
         },
      });
   })
}