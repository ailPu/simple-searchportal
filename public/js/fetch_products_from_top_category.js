// Get GET_Parameters
const queryStringAtTopLevel = window.location.search;
const urlParamsAtTopLevel = new URLSearchParams(queryStringAtTopLevel);
const currentcategory_pathAtTopLevel = urlParamsAtTopLevel.get("category_path");


if(!currentcategory_pathAtTopLevel){
   if ($("form").length > 0) {
      console.log($("form#search input[name=keyword]"));
         
      $("form#search input[name=keyword]").on("keyup", function(e) {
         if ($(this).val().length < 3) {
            $(".results").empty();
            return;
         }
         
         const searchRoute = $("form#search").attr("action");
         const keyword = $(this).val();
         
         $.ajax({
            url: searchRoute,
            method: "GET",
            data: {
               "keyword": $(this).val(),
               "atTopLevel": true,
            },
            dataType: "json",
            success: function(results) {
               console.log(results);
               //! Alte Variante 
               // $("form#search input[name=keyword]").nextAll().remove();
               $(".results").empty();
               if(!results.length){
                  console.log("Keine Treffer");
                  return;
               }
               // $("form#search input[name=keyword]").nextAll().remove();
               let searchResults = $("<ul></ul>");
               results.forEach((result) => {
                  searchResults.append(`<li><a href=${searchRoute}?category_path=${result.category_path}&currCatId=${result.first_level_category_id}&currLevel=${result.currLevel}&keyword=${keyword}>${result.cat_count} Treffer <span>in ${result.first_level_category_name}</span></a></li>`);
               });
               //! Alte Variante 
               // $("form#search input[name=keyword]").after(searchResults)
               $(".results").html(searchResults)
            },
         });
      })
   }
}
