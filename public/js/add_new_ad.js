$("form#add-new-ad").on("submit", function(e) {

      if (!$("input#ad-name").val()) {
         alert("Titel darf nicht leer sein");
         e.preventDefault();
         return;
      }

      if (!$("input#ad-price").val()) {
         alert("Zahl vor dem Komma darf nicht leer sein");
         e.preventDefault();
         return;
      }

      if (!$("input#ad-price-comma").val()) {
         alert("Zahl nach dem Komma darf nicht leer sein");
         e.preventDefault();
         return;
      }

      $("form#add-new-ad .categories-container select").each(function(idx,option) {
         if ($(this).val() === "") {
            e.preventDefault();
            alert("Bitte Kategorien auswählen!");
            return;
         }
      });

      if($("#attribute_colors input").length){
         let colorSelected = false;
         $("#attribute_colors input").each(function(idx,radio){
            if($(radio).prop("checked")){
               colorSelected = true;
            };
         })
         if(!colorSelected){ 
            e.preventDefault();
            alert("Bitte Farbe auswählen!");
            return;
         }  
      }

      if($("#attribute_sizes input").length){
         let sizeSelected = false;
         $("#attribute_sizes input").each(function(idx,radio){
            if($(radio).prop("checked")){
               sizeSelected = true;
            };
         })
         if(!sizeSelected){
            e.preventDefault();
            alert("Bitte Größe auswählen!");
            return;
         }
      }

      if($("#attribute_shoe_sizes input").length){  
         let shoeSizeSelected = false;
         $("#attribute_shoe_sizes input").each(function(idx,radio){
            if($(radio).prop("checked")){
               shoeSizeSelected = true;
            };
         })
         if(!shoeSizeSelected){
            e.preventDefault();
            alert("Bitte Schuhgöße auswählen!");
            return;
         }
      }

      if($("#attribute_waist_sizes input").length){  
         let waistSizeSelected = false;
         $("#attribute_waist_sizes input").each(function(idx,radio){
            if($(radio).prop("checked")){
               waistSizeSelected = true;
            };
         })
         if(!waistSizeSelected){
            e.preventDefault();
            alert("Bitte Bundweite auswählen!");
            return;
         }
      }

      if($("#attribute_conditions input").length){  
         let conditionSelected = false;
         $("#attribute_conditions input").each(function(idx,radio){
            if($(radio).prop("checked")){
               conditionSelected = true;
            };
         })
         if(!conditionSelected){
            e.preventDefault();
            alert("Bitte Zustand auswählen!");
            return;
         }
      }

      $("select#districts").each(function(idx,option) {
         if ($(this).val() === "") {
            e.preventDefault();
            alert("Bitte Verkaufsort auswählen!");
            return;
         }
      });

      console.log("HELLO");
   })