<?php

use App\Events\MessageSent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SearchController@index')->name("search.index");

// Adding new ad
Route::get('/fetch-subcategories', 'AdController@fetchSubcategories')->name("ad.getSubcategories");
Route::get('/fetch-attributes', 'AdController@fetchAttributes')->name("ad.getAttributes");
Route::get('/select-imgs/{ad}', 'AdController@selectImgs')->name("ad.selectImgs");
Route::post('/store-imgs/{ad}', 'AdController@storeImgs')->name("ad.storeImgs");
Route::get('/show-preview/{ad}', 'AdController@showPreview')->name("ad.showPreview");
// TODO redirect->action would suffice(?)
Route::put('/release/{ad}', 'AdController@releaseAd')->name("ad.release");

// Editing Ad
Route::get('/edit-imgs/{ad}', 'AdController@editImgs')->name("ad.editImgs");
// TODO redirect->action would suffice(?)
Route::put('/update-imgs/{ad}', 'AdController@updateImgs')->name("ad.updateImgs");
Route::resource('/ad', 'AdController');


Route::resource('/user', 'UserController');

Route::get('/search-results', 'SearchController@results')->name("search.getResults");

// IMG Route
Route::get('/fetch-image/{image}', 'AdController@outputImg')->name('ad.outputImg');

Auth::routes();

Route::get('/check-login-status', 'UserController@checkLoginStatus')->name("user.check_login_status");
Route::resource("/user", "UserController");

Route::get('/home', 'HomeController@index')->name('home');


// CHAT
Broadcast::routes();
Route::get('/chat', 'ChatController@index')->name("chat");
Route::get('/users', 'UserController@fetchUsers')->name("chat.fetchUsers");
Route::get('/messages/{user}', 'ChatController@fetchMessages');
Route::post('/messages/{user}', 'ChatController@sendMessage');
